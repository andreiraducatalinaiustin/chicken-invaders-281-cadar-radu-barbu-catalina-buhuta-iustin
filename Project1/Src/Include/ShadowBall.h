#pragma once
#include "Object.h"

class ShadowBall :public Object {
public:
	ShadowBall(float coordx, float coordy, float dx, float dy);
	void Update();
	void Draw();
private:
	std::string ShadowBallTexturePath;
};