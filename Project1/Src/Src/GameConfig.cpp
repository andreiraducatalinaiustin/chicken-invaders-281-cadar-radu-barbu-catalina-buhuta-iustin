#include "GameConfig.h"
#include <fstream>
#include <iostream>
#include <algorithm>

GameConfig::GameConfig()
{
	std::ifstream file("GameConfig.cfg");
	if (!file.is_open())
		std::cout << "GameConfig could not be opened!\n";
	std::string line;
	while (std::getline(file, line)) {
		line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
		if (line[0] == '#' || line.empty())
			continue;
		auto delimiterPos = line.find("=");
		auto key = line.substr(0, delimiterPos);
		auto value = line.substr(delimiterPos + 1);
		m_data[key] = value;
	}
	file.close();
}

GameConfig::GameConfig(const std::string& filePath)
{
	std::ifstream file(filePath);
	if (!file.is_open())
		std::cout << "GameConfig could not be opened!\n";
	std::string line;
	while (std::getline(file, line)) {
		line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
		if (line[0] == '#' || line.empty())
			continue;
		auto delimiterPos = line.find("=");
		auto key = line.substr(0, delimiterPos);
		auto value = line.substr(delimiterPos + 1);
		m_data[key] = value;
	}
	file.close();
}

GameConfig::~GameConfig()
{
}
