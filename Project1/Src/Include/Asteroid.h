#pragma once
#include <SFML/Graphics.hpp>
#include "Object.h"
class Asteroid : public Object
{
public:
	Asteroid(int mod);
	Asteroid(float coordx, float coordy, float radius, float angle, float health);
	void Update();
	void Draw();
private:
	int m_textureX;
	int m_textureY;
	int m_mod;
	std::string AsteroidTexturePath;
};