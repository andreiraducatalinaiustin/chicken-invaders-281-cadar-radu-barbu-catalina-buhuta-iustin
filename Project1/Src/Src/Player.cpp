#include "..\Include\Player.h"
#include "MusicPlayer.h"

Player::Player()
{
	m_name = ObjectName::PLAYER;
	m_bulletType = BulletType::BLUEBULLET;

	m_health = 3;
	m_invincibleDuration = 3;

	m_blueBulletLevel = 0;
	m_redBulletLevel = 0;
	m_greenBulletLevel = 0;

	m_maxBlueBulletLevel = 6;
	m_maxRedBulletLevel = 8;
	m_maxGreenBulletLevel = 10;

	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();

	m_gameConfig->get("PlayerTexturePath", PlayerTexturePath);
	m_gameConfig->get("PlayerTextureLeftPath", PlayerTextureLeftPath);
	m_gameConfig->get("PlayerTextureRightPath", PlayerTextureRightPath);
	m_gameConfig->get("PlayerTextureUpFlamesPath", PlayerTextureUpFlamesPath);
	m_gameConfig->get("PlayerTextureLeftFlamesPath", PlayerTextureLeftFlamesPath);
	m_gameConfig->get("PlayerTextureRightFlamesPath", PlayerTextureRightFlamesPath);
	m_gameConfig->get("InvincibleTexturePath", InvincibleTexturePath);


	PlayerTexture.loadFromFile(PlayerTexturePath);
	PlayerTextureLeft.loadFromFile(PlayerTextureLeftPath);
	PlayerTextureRight.loadFromFile(PlayerTextureRightPath);
	PlayerTextureUpFlames.loadFromFile(PlayerTextureUpFlamesPath);
	PlayerTextureLeftFlames.loadFromFile(PlayerTextureLeftFlamesPath);
	PlayerTextureRightFlames.loadFromFile(PlayerTextureRightFlamesPath);
	InvincibleTexture.loadFromFile(InvincibleTexturePath);
		
	PlayerTexture.setSmooth(true);
	PlayerTextureLeft.setSmooth(true);
 	PlayerTextureRight.setSmooth(true);
	PlayerTextureUpFlames.setSmooth(true);
	PlayerTextureLeftFlames.setSmooth(true);
	PlayerTextureRightFlames.setSmooth(true);
	InvincibleTexture.setSmooth(true);

	m_spritePlayer.setTexture(PlayerTexture);
	m_spritePlayerLeft.setTexture(PlayerTextureLeft);
	m_spritePlayerRight.setTexture(PlayerTextureRight);
	m_spritePlayerUpFlames.setTexture(PlayerTextureUpFlames);
	m_spritePlayerLeftFlames.setTexture(PlayerTextureLeftFlames);
	m_spritePlayerRightFlames.setTexture(PlayerTextureRightFlames);
	m_spriteInvincible.setTexture(InvincibleTexture);
	
	m_sprite = m_spritePlayer;
	Settings(WIDTH / 2, HEIGHT / 2, 90);
	m_sprite.setPosition(m_coordx, m_coordy);
	m_radius = PlayerTexture.getSize().y / 2.0f;
}

void Player::Fire(std::list<Object*>& objects)
{
	switch (m_bulletType)
	{
	case BulletType::BLUEBULLET:
		switch (m_blueBulletLevel)
		{
		case 0:
		{
			Bullet* blueBullet = new Bullet();
			blueBullet->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07,
				m_coordy, 270, 10);
			objects.push_back(blueBullet);
		}
		break;
		case 1:
		{
			Bullet* blueBullet = new Bullet();

			blueBullet->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 - 10,
				m_coordy, 270, 10);
			objects.push_back(blueBullet);

			Bullet* blueBullet2 = new Bullet();
			blueBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 + 10,
				m_coordy, 270, 10);
			objects.push_back(blueBullet2);
		}
		break;
		case 2:
		{
			Bullet* blueBullet = new Bullet();

			blueBullet->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 - 20,
				m_coordy, 270, 10);
			objects.push_back(blueBullet);

			Bullet* blueBullet2 = new Bullet();
			blueBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07,
				m_coordy - 10, 270, 10);
			objects.push_back(blueBullet2);

			Bullet* blueBullet3 = new Bullet();
			blueBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 + 20,
				m_coordy, 270, 10);
			objects.push_back(blueBullet3);
		}
		break;
		case 3:
		{
			Bullet* blueBullet = new Bullet();

			blueBullet->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 - 20,
				m_coordy, 263, 10);
			blueBullet->SetRotatedLeftTexture();
			objects.push_back(blueBullet);

			Bullet* blueBullet2 = new Bullet();
			blueBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07,
				m_coordy - 10, 270, 10);
			objects.push_back(blueBullet2);

			Bullet* blueBullet3 = new Bullet();
			blueBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 + 20,
				m_coordy, 277, 10);
			blueBullet3->SetRotatedRightTexture();
			objects.push_back(blueBullet3);

		}
		break;
		case 4:
		{
			Bullet* blueBullet = new Bullet();

			blueBullet->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 - 10,
				m_coordy, 263, 10);
			blueBullet->SetRotatedLeftTexture();
			objects.push_back(blueBullet);

			Bullet* blueBullet2 = new Bullet();
			blueBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 - 30,
				m_coordy, 263, 10);
			blueBullet2->SetRotatedLeftTexture();
			objects.push_back(blueBullet2);

			Bullet* blueBullet3 = new Bullet();
			blueBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 + 10,
				m_coordy, 277, 10);
			blueBullet3->SetRotatedRightTexture();
			objects.push_back(blueBullet3);

			Bullet* blueBullet4 = new Bullet();
			blueBullet4->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 + 30,
				m_coordy, 277, 10);
			blueBullet4->SetRotatedRightTexture();
			objects.push_back(blueBullet4);
		}
		break;
		case 5:
		{
			Bullet* blueBullet = new Bullet();

			blueBullet->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 - 10,
				m_coordy, 263, 10);
			blueBullet->SetRotatedLeftTexture();
			objects.push_back(blueBullet);

			Bullet* blueBullet2 = new Bullet();
			blueBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 - 30,
				m_coordy, 263, 10);
			blueBullet2->SetRotatedLeftTexture();
			objects.push_back(blueBullet2);

			Bullet* blueBullet3 = new Bullet();
			blueBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07,
				m_coordy, 270, 10);
			objects.push_back(blueBullet3);

			Bullet* blueBullet4 = new Bullet();
			blueBullet4->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 + 10,
				m_coordy, 277, 10);
			blueBullet4->SetRotatedRightTexture();
			objects.push_back(blueBullet4);

			Bullet* blueBullet5 = new Bullet();
			blueBullet5->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 + 30,
				m_coordy, 277, 10);
			blueBullet5->SetRotatedRightTexture();
			objects.push_back(blueBullet5);
		}
		break;
		case 6:
		{
			Bullet* blueBullet = new Bullet();

			blueBullet->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 - 30,
				m_coordy, 255, 10);
			blueBullet->SetRotatedLeftTexture();
			objects.push_back(blueBullet);

			Bullet* blueBullet2 = new Bullet();
			blueBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 - 10,
				m_coordy, 263, 10);
			blueBullet2->SetRotatedLeftTexture();
			objects.push_back(blueBullet2);

			Bullet* extraLeftBullet = new Bullet();
			extraLeftBullet->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 - 5,
				m_coordy, 267, 10);
			extraLeftBullet->SetRotatedLeftTexture();
			objects.push_back(extraLeftBullet);

			Bullet* blueBullet3 = new Bullet();
			blueBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07,
				m_coordy, 270, 10);
			objects.push_back(blueBullet3);

			Bullet* extraRightBullet = new Bullet();
			extraRightBullet->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 + 5,
				m_coordy, 273, 10);
			extraRightBullet->SetRotatedRightTexture();
			objects.push_back(extraRightBullet);

			Bullet* blueBullet4 = new Bullet();
			blueBullet4->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 + 10,
				m_coordy, 277, 10);
			blueBullet4->SetRotatedRightTexture();
			objects.push_back(blueBullet4);

			Bullet* blueBullet5 = new Bullet();
			blueBullet5->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.07 + 30,
				m_coordy, 285, 10);
			blueBullet5->SetRotatedRightTexture();
			objects.push_back(blueBullet5);
		}
		break;
		default:
			break;
		}
		break;

	case BulletType::REDBULLET:
	{
		switch (m_redBulletLevel)
		{
		case 0:
		{
			SingleRedBullet* redBullet = new SingleRedBullet();
			redBullet->Settings(m_coordx + m_sprite.getLocalBounds().width*0.125,
				m_coordy -100 , 270, 10);
			objects.push_back(redBullet);
		}
		break;
		case 1:
		{
			SingleRedBullet* redBullet = new SingleRedBullet();
			redBullet->Settings(m_coordx - m_sprite.getLocalBounds().width/4 ,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet);
			SingleRedBullet* redBullet1 = new SingleRedBullet();
			redBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width/2,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet1);
		}
		break;
		case 2:
		{
			SingleRedBullet* redBullet = new SingleRedBullet();
			redBullet->Settings(m_coordx - m_sprite.getLocalBounds().width / (4-1/4),
				m_coordy - 80, 270, 10);
			objects.push_back(redBullet);
			SingleRedBullet* redBullet1 = new SingleRedBullet();
			redBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width*1/8,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet1);
			SingleRedBullet* redBullet2 = new SingleRedBullet();
			redBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width / (5/2),
				m_coordy - 80, 270, 10);
			objects.push_back(redBullet2);
		}
		break;
		case 3:
		{
			SingleRedBullet* redBullet = new SingleRedBullet();
			redBullet->Settings(m_coordx + m_sprite.getLocalBounds().width *1/8-20,
				m_coordy-80, 263, 10);
			redBullet->SetRotatedLeftTexture();
			objects.push_back(redBullet);
			SingleRedBullet* redBullet1 = new SingleRedBullet();
			redBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 1 / 8,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet1);
			SingleRedBullet* redBullet2 = new SingleRedBullet();
			redBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width*1/8 +10,
				m_coordy - 80, 277, 10);
			redBullet2->SetRotatedRightTexture();
			objects.push_back(redBullet2);
		}
		break;
		case 4:
		{
			SingleRedBullet* redBullet = new SingleRedBullet();
			redBullet->Settings(m_coordx - m_sprite.getLocalBounds().width*1/8 -20,
				m_coordy - 80, 263, 10);
			redBullet->SetRotatedLeftTexture();
			objects.push_back(redBullet);
			SingleRedBullet* redBullet1 = new SingleRedBullet();
			redBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 1 / 8 - 10,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet1);
			SingleRedBullet* redBullet2 = new SingleRedBullet();
			redBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 1 / 8 + 10,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet2);
			SingleRedBullet* redBullet3 = new SingleRedBullet();
			redBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width *1/8 +20,
				m_coordy - 80, 277, 10);
			redBullet3->SetRotatedRightTexture();
			objects.push_back(redBullet3);
		}
		break;
		case 5:
		{
			SingleRedBullet* redBullet = new SingleRedBullet();
			redBullet->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 - 30,
				m_coordy - 80, 263, 10);
			redBullet->SetRotatedLeftTexture();
			objects.push_back(redBullet);

			SingleRedBullet* redBullet1 = new SingleRedBullet();
			redBullet1->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 - 10,
				m_coordy - 90, 263, 10);
			redBullet1->SetRotatedLeftTexture();
			objects.push_back(redBullet1);

			SingleRedBullet* redBullet2 = new SingleRedBullet();
			redBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 1 / 8 ,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet2);

			SingleRedBullet* redBullet3 = new SingleRedBullet();
			redBullet3->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 + 15,
				m_coordy - 90, 277, 10);
			redBullet3->SetRotatedRightTexture();
			objects.push_back(redBullet3);

			SingleRedBullet* redBullet4 = new SingleRedBullet();
			redBullet4->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 + 40,
				m_coordy - 80, 277, 10);
			redBullet4->SetRotatedRightTexture();
			objects.push_back(redBullet4);
		}
		break;
		case 6:
		{
			SingleRedBullet* redBullet = new SingleRedBullet();
			redBullet->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 - 40,
				m_coordy - 80, 263, 10);
			redBullet->SetRotatedLeftTexture();
			objects.push_back(redBullet);

			SingleRedBullet* redBullet1 = new SingleRedBullet();
			redBullet1->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 - 20,
				m_coordy - 90, 263, 10);
			redBullet1->SetRotatedLeftTexture();
			objects.push_back(redBullet1);

			SingleRedBullet* redBullet2 = new SingleRedBullet();
			redBullet2->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 - 7,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet2);

			SingleRedBullet* redBullet3 = new SingleRedBullet();
			redBullet3->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 + 23,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet3);

			SingleRedBullet* redBullet4 = new SingleRedBullet();
			redBullet4->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 + 27,
				m_coordy - 90, 277, 10);
			redBullet4->SetRotatedRightTexture();
			objects.push_back(redBullet4);

			SingleRedBullet* redBullet5 = new SingleRedBullet();
			redBullet5->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 + 45,
				m_coordy - 80, 277, 10);
			redBullet5->SetRotatedRightTexture();
			objects.push_back(redBullet5);
		}
		break;
		case 7:
		{
			SingleRedBullet* redBullet = new SingleRedBullet();
			redBullet->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 - 53,
				m_coordy - 70, 263, 10);
			redBullet->SetRotatedLeftTexture();
			objects.push_back(redBullet);

			SingleRedBullet* redBullet1 = new SingleRedBullet();
			redBullet1->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 - 33,
				m_coordy - 80, 263, 10);
			redBullet1->SetRotatedLeftTexture();
			objects.push_back(redBullet1);

			SingleRedBullet* redBullet2 = new SingleRedBullet();
			redBullet2->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 - 13,
				m_coordy - 90, 263, 10);
			redBullet2->SetRotatedLeftTexture();
			objects.push_back(redBullet2);

			SingleRedBullet* redBullet3 = new SingleRedBullet();
			redBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 1 / 8,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet3);

			SingleRedBullet* redBullet4 = new SingleRedBullet();
			redBullet4->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 + 20,
				m_coordy - 90, 277, 10);
			redBullet4->SetRotatedRightTexture();
			objects.push_back(redBullet4);

			SingleRedBullet* redBullet5 = new SingleRedBullet();
			redBullet5->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 + 40,
				m_coordy - 80, 277, 10);
			redBullet5->SetRotatedRightTexture();
			objects.push_back(redBullet5);

			SingleRedBullet* redBullet6 = new SingleRedBullet();
			redBullet6->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 + 60,
				m_coordy - 70, 277, 10);
			redBullet6->SetRotatedRightTexture();
			objects.push_back(redBullet6);
		}
		break;
		case 8: 
		{
			SingleRedBullet* redBullet = new SingleRedBullet();
			redBullet->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 - 70,
				m_coordy - 70, 263, 10);
			redBullet->SetRotatedLeftTexture();
			objects.push_back(redBullet);

			SingleRedBullet* redBullet1 = new SingleRedBullet();
			redBullet1->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 - 50,
				m_coordy - 80, 263, 10);
			redBullet1->SetRotatedLeftTexture();
			objects.push_back(redBullet1);

			SingleRedBullet* redBullet2 = new SingleRedBullet();
			redBullet2->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 - 30,
				m_coordy - 90, 263, 10);
			redBullet2->SetRotatedLeftTexture();
			objects.push_back(redBullet2);

			SingleRedBullet* redBullet3 = new SingleRedBullet();
			redBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 1 / 8 - 10,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet3);

			SingleRedBullet* redBullet4 = new SingleRedBullet();
			redBullet4->Settings(m_coordx + m_sprite.getLocalBounds().width * 1 / 8 + 10,
				m_coordy - 100, 270, 10);
			objects.push_back(redBullet4);

			SingleRedBullet* redBullet5 = new SingleRedBullet();
			redBullet5->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 + 40,
				m_coordy - 90, 277, 10);
			redBullet5->SetRotatedRightTexture();
			objects.push_back(redBullet5);

			SingleRedBullet* redBullet6 = new SingleRedBullet();
			redBullet6->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 + 60,
				m_coordy - 80, 277, 10);
			redBullet6->SetRotatedRightTexture();
			objects.push_back(redBullet6);

			SingleRedBullet* redBullet7 = new SingleRedBullet();
			redBullet7->Settings(m_coordx - m_sprite.getLocalBounds().width * 1 / 8 + 80,
				m_coordy - 70, 277, 10);
			redBullet7->SetRotatedRightTexture();
			objects.push_back(redBullet7);

		}
		break;
		}

		break;
	}

	case BulletType::GREENBULLET:
	{
		switch (m_greenBulletLevel)
		{
		case 0:
		{
			SingleGreenBullet* greenBullet = new SingleGreenBullet();
			greenBullet->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2,
				m_coordy, 270, 10);
			objects.push_back(greenBullet);
		}
		break;
		case 1:
		{
			SingleGreenBullet* greenBullet1 = new SingleGreenBullet();
			greenBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 10,
				m_coordy, 270, 10);
			objects.push_back(greenBullet1);

			SingleGreenBullet* greenBullet2 = new SingleGreenBullet();
			greenBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 10,
				m_coordy, 270, 10);
			objects.push_back(greenBullet2);
		}
		break;
		case 2:
		{
			SingleGreenBullet* greenBullet1 = new SingleGreenBullet();
			greenBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 20,
				m_coordy, 270, 10);
			objects.push_back(greenBullet1);

			SingleGreenBullet* greenBullet2 = new SingleGreenBullet();
			greenBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2,
				m_coordy - 15, 270, 10);
			objects.push_back(greenBullet2);

			SingleGreenBullet* greenBullet3 = new SingleGreenBullet();
			greenBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 20,
				m_coordy, 270, 10);
			objects.push_back(greenBullet3);
		}
		break;
		case 3:
		{
			SingleGreenBullet* greenBullet1 = new SingleGreenBullet();
			greenBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 20,
				m_coordy, 270, 10);
			objects.push_back(greenBullet1);

			SingleGreenBullet* greenBullet2 = new SingleGreenBullet();
			greenBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2,
				m_coordy - 15, 270, 10);

			greenBullet2->SetMediumTexture();
			greenBullet2->SetMediumDamage();

			objects.push_back(greenBullet2);

			SingleGreenBullet* greenBullet3 = new SingleGreenBullet();
			greenBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 40,
				m_coordy, 270, 10);
			objects.push_back(greenBullet3);
		
		}
		break;
		case 4:
		{
			SingleGreenBullet* greenBullet1 = new SingleGreenBullet();
			greenBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 20,
				m_coordy, 270, 10);

			greenBullet1->SetMediumTexture();
			greenBullet1->SetMediumDamage();

			objects.push_back(greenBullet1);

			SingleGreenBullet* greenBullet2 = new SingleGreenBullet();
			greenBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 20,
				m_coordy, 270, 10);

			greenBullet2->SetMediumTexture();
			greenBullet2->SetMediumDamage();

			objects.push_back(greenBullet2);
		}
		break;
		case 5:
		{
			SingleGreenBullet* greenBullet1 = new SingleGreenBullet();
			greenBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 25,
				m_coordy, 270, 10);

			greenBullet1->SetMediumTexture();
			greenBullet1->SetMediumDamage();

			objects.push_back(greenBullet1);

			SingleGreenBullet* greenBullet2 = new SingleGreenBullet();
			greenBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2,
				m_coordy - 15, 270, 10);

			greenBullet2->SetMediumTexture();
			greenBullet2->SetMediumDamage();

			objects.push_back(greenBullet2);

			SingleGreenBullet* greenBullet3 = new SingleGreenBullet();
			greenBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 25,
				m_coordy, 270, 10);

			greenBullet3->SetMediumTexture();
			greenBullet3->SetMediumDamage();

			objects.push_back(greenBullet3);
		}
		break;
		case 6:
		{
			SingleGreenBullet* greenBullet1 = new SingleGreenBullet();
			greenBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 40,
				m_coordy, 270, 10);
			objects.push_back(greenBullet1);

			SingleGreenBullet* greenBullet2 = new SingleGreenBullet();
			greenBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 20,
				m_coordy - 20, 270, 10);

			greenBullet2->SetStrongTexture();
			greenBullet2->SetStrongDamage();

			objects.push_back(greenBullet2);

			SingleGreenBullet* greenBullet3 = new SingleGreenBullet();
			greenBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 40,
				m_coordy, 270, 10);
			objects.push_back(greenBullet3);
		}
		break;
		case 7:
		{
			SingleGreenBullet* greenBullet1 = new SingleGreenBullet();
			greenBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 45,
				m_coordy, 270, 10);

			greenBullet1->SetMediumTexture();
			greenBullet1->SetMediumDamage();

			objects.push_back(greenBullet1);

			SingleGreenBullet* greenBullet2 = new SingleGreenBullet();
			greenBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 20,
				m_coordy - 20, 270, 10);

			greenBullet2->SetStrongTexture();
			greenBullet2->SetStrongDamage();

			objects.push_back(greenBullet2);

			SingleGreenBullet* greenBullet3 = new SingleGreenBullet();
			greenBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 30,
				m_coordy, 270, 10);

			greenBullet3->SetMediumTexture();
			greenBullet3->SetMediumDamage();

			objects.push_back(greenBullet3);
		}
		break;
		case 8:
		{
			SingleGreenBullet* greenBullet1 = new SingleGreenBullet();
			greenBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 50,
				m_coordy, 270, 10);
			objects.push_back(greenBullet1);

			SingleGreenBullet* greenBullet2 = new SingleGreenBullet();
			greenBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 30,
				m_coordy - 20, 270, 10);

			greenBullet2->SetMediumTexture();
			greenBullet2->SetMediumDamage();
			objects.push_back(greenBullet2);

			SingleGreenBullet* greenBullet3 = new SingleGreenBullet();
			greenBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2,
				m_coordy - 35, 270, 10);

			greenBullet3->SetMediumTexture();
			greenBullet3->SetMediumDamage();
			objects.push_back(greenBullet3);

			SingleGreenBullet* greenBullet4 = new SingleGreenBullet();
			greenBullet4->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 30,
				m_coordy - 20, 270, 10);

			greenBullet4->SetMediumTexture();
			greenBullet4->SetMediumDamage();
			objects.push_back(greenBullet4);

			SingleGreenBullet* greenBullet5 = new SingleGreenBullet();
			greenBullet5->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 65,
				m_coordy, 270, 10);
			objects.push_back(greenBullet5);
		}
		break;
		case 9:
		{
			SingleGreenBullet* greenBullet1 = new SingleGreenBullet();
			greenBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 70,
				m_coordy, 270, 10);
			objects.push_back(greenBullet1);

			SingleGreenBullet* greenBullet2 = new SingleGreenBullet();
			greenBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 50,
				m_coordy - 20, 270, 10);

			greenBullet2->SetMediumTexture();
			greenBullet2->SetMediumDamage();
			objects.push_back(greenBullet2);

			SingleGreenBullet* greenBullet3 = new SingleGreenBullet();
			greenBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 20,
				m_coordy - 35, 270, 10);

			greenBullet3->SetStrongTexture();
			greenBullet3->SetStrongDamage();
			objects.push_back(greenBullet3);

			SingleGreenBullet* greenBullet4 = new SingleGreenBullet();
			greenBullet4->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 30,
				m_coordy - 20, 270, 10);

			greenBullet4->SetMediumTexture();
			greenBullet4->SetMediumDamage();
			objects.push_back(greenBullet4);

			SingleGreenBullet* greenBullet5 = new SingleGreenBullet();
			greenBullet5->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 65,
				m_coordy, 270, 10);
			objects.push_back(greenBullet5);
		}
		break;
		case 10:
		{
			SingleGreenBullet* greenBullet1 = new SingleGreenBullet();
			greenBullet1->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 80,
				m_coordy, 270, 10);

			greenBullet1->SetStrongTexture();
			greenBullet1->SetStrongDamage();
			objects.push_back(greenBullet1);

			SingleGreenBullet* greenBullet2 = new SingleGreenBullet();
			greenBullet2->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 45,
				m_coordy - 25, 270, 10);

			greenBullet2->SetMediumTexture();
			greenBullet2->SetMediumDamage();
			objects.push_back(greenBullet2);

			SingleGreenBullet* greenBullet3 = new SingleGreenBullet();
			greenBullet3->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 - 20,
				m_coordy - 50, 270, 10);

			greenBullet3->SetStrongTexture();
			greenBullet3->SetStrongDamage();
			objects.push_back(greenBullet3);

			SingleGreenBullet* greenBullet4 = new SingleGreenBullet();
			greenBullet4->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 30,
				m_coordy - 20, 270, 10);

			greenBullet4->SetMediumTexture();
			greenBullet4->SetMediumDamage();
			objects.push_back(greenBullet4);

			SingleGreenBullet* greenBullet5 = new SingleGreenBullet();
			greenBullet5->Settings(m_coordx + m_sprite.getLocalBounds().width * 0.2 + 55,
				m_coordy, 270, 10);

			greenBullet5->SetStrongTexture();
			greenBullet5->SetStrongDamage();
			objects.push_back(greenBullet5);
		}
		break;
		default:
			break;
		}
	}

	}
}

void Player::SetThrust(bool thrust)
{
	m_thrust = thrust;
}

bool Player::GetThrust()
{
	return m_thrust;
	m_sprite.setPosition(m_coordx, m_coordy);
}

bool Player::GetInvincible()
{
	return m_isInvincible;
}

uint16_t Player::GetInvincibleDuration()
{
	return m_invincibleDuration;
}

void Player::incrementBulletLevel(BulletType bulletType)
{
	switch (bulletType)
	{
	   case BulletType::BLUEBULLET:
	   {
		   ++m_blueBulletLevel;
	   }
	   break;
	   case BulletType::REDBULLET:
	   {
		   ++m_redBulletLevel;
	   }
	   break;
	   case BulletType::GREENBULLET:
	   {
		   ++m_greenBulletLevel;
	   }
	   break;
	}
}


BulletType Player::getBulletType()
{
	return m_bulletType;
}

uint8_t Player::getBlueBulletLevel()
{
	return m_blueBulletLevel;
}

uint8_t Player::getRedBulletLevel()
{
	return m_redBulletLevel;
}

uint8_t Player::getGreenBulletLevel()
{
	return m_greenBulletLevel;
}


uint8_t Player::getMaxBlueBulletLevel()
{
	return m_maxBlueBulletLevel;
}

uint8_t Player::getMaxRedBulletLevel()
{
	return m_maxRedBulletLevel;
}

uint8_t Player::getMaxGreenBulletLevel()
{
	return m_maxGreenBulletLevel;
}

sf::Clock& Player::GetClock()
{
	return m_clock;
}

void Player::Update()
{
	if (m_thrust)
	{
		m_coordx += m_velocity.x;
		m_coordy += m_velocity.y;
	}

	if (m_isInvincible)
	{
		if (m_clock.getElapsedTime().asSeconds() > m_invincibleDuration)
			m_isInvincible = false;
	}
	
	if (m_coordx + m_sprite.getGlobalBounds().width >= WIDTH)
		m_coordx = WIDTH - m_sprite.getGlobalBounds().width;

	if (m_coordx < 0)
		m_coordx = 0;

	if (m_coordy + m_sprite.getGlobalBounds().height >= HEIGHT)
		m_coordy = HEIGHT - m_sprite.getGlobalBounds().height;

	if (m_coordy < 0)
		m_coordy = 0;

	m_sprite.setPosition(m_coordx, m_coordy);
	m_spriteInvincible.setPosition(m_coordx - m_sprite.getGlobalBounds().width / 2 -4, m_coordy - m_sprite.getGlobalBounds().height / 2);
	
}

void Player::Draw()
{
}

void Player::SetInvincible(bool invincible)
{
	m_isInvincible = invincible;
}

void Player::SetSpriteDefault()
{
	m_sprite = m_spritePlayer;
}


void Player::SetSpriteLeft()
{
	m_sprite = m_spritePlayerLeft;
}

void Player::SetSpriteRight()
{
	m_sprite = m_spritePlayerRight;
}

void Player::SetSpriteUpFlames()
{
	m_sprite = m_spritePlayerUpFlames;
}

void Player::SetSpriteLeftFlames()
{
	m_sprite = m_spritePlayerLeftFlames;
}

void Player::SetSpriteRightFlames()
{
	m_sprite = m_spritePlayerRightFlames;
}

void Player::SetAngle(uint16_t angle)
{
	m_playerAngle = angle;
}

void Player::SetBulletType(BulletType bulletType)
{
	m_bulletType = bulletType;
}

sf::Vector2f Player::getVelocity()const
{
	return m_velocity;
}

void Player::setVelocity(sf::Vector2f velocity)
{
	m_velocity = velocity;
}

const float Player::getMovementSpeed() const
{
	return m_movementSpeed;
}

void Player::CoordonateSettings(float coordx, float coordy)
{
	m_coordx = coordx;
	m_coordy = coordy;

	if (m_coordx > WIDTH)
		m_coordx = WIDTH;

	if (m_coordx < 0)
		m_coordx = 0;

	if (m_coordy > HEIGHT)
		m_coordy = HEIGHT;

	if (m_coordy < 0)
		m_coordy = 0;
}

sf::Sprite Player::getSpriteInvincible()
{
	return m_spriteInvincible;
}
