#include "..\Include\Bullet.h"
#include "Object.h"
Bullet::Bullet()
{
	m_name = ObjectName::BULLET;

	m_frame = 0;
	m_health = 1;
	m_damage = 1;

	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();

	m_gameConfig->get("SingleBlueBulletPath", SingleBlueBulletPath);
	m_gameConfig->get("SingleBlueBulletRotatedRightPath", singleBlueBulletRotatedRightPath);
	m_gameConfig->get("SingleBlueBulletRotatedLeftPath", singleBlueBulletRotatedLeftPath);

	m_texture.loadFromFile(SingleBlueBulletPath);
	m_textureRotatedRight.loadFromFile(singleBlueBulletRotatedRightPath);
	m_textureRotatedLeft.loadFromFile(singleBlueBulletRotatedLeftPath);

	m_sprite.setTexture(m_texture);
}



void Bullet::Update()
{


	m_dx = cos(m_angle * RADIAN) * 6;
	m_dy = sin(m_angle * RADIAN) * 6;
	m_coordx += m_dx;
	m_coordy += m_dy;
	

	if (m_coordx > WIDTH || m_coordx<0 || m_coordy> HEIGHT || m_coordy < 0) 
  		m_health = 0;


	m_sprite.setPosition(m_coordx, m_coordy);
	
}

void Bullet::Draw()
{

}

void Bullet::SetRotatedRightTexture()
{
	m_sprite.setTexture(m_textureRotatedRight);
}

void Bullet::SetRotatedLeftTexture()
{
	m_sprite.setTexture(m_textureRotatedLeft);
}


