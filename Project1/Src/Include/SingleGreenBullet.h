#pragma once
#include "Object.h"
class SingleGreenBullet : public Object
{
public:
	SingleGreenBullet();
	void Update();
	void Draw();

	void SetMediumDamage();
	void SetMediumTexture();

	void SetStrongDamage();
	void SetStrongTexture();

private:
	std::string SingleGreenBulletPath, SingleGreenBulletMediumPath, SingleGreenBulletStrongPath;
	sf::Texture m_textureMedium, m_textureStrong;
	sf::Sprite m_mediumSprite, m_strongSprite;
};