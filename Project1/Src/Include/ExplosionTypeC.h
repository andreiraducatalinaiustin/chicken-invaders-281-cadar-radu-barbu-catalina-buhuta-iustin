#pragma once
#include"Object.h"
class ExplosionTypeC :public Object
{
public:
	ExplosionTypeC(float coordx, float coordy);
	void Update();
	void Draw();
private:
	std::string TexturePath;
	double m_speed, m_frame;
	std::vector<sf::IntRect> m_frames;

};