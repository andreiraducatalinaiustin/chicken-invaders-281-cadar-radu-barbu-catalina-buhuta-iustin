#include "Animation.h"

Animation::Animation()
{
}



Animation::Animation(float coordX, float coordY, uint8_t numFrames, float speed, float width, float height, sf::Texture texture)
	:m_speed(speed), m_frame(0), m_sprite(texture)
{
	for (int index = 0; index < numFrames; index++)
		m_frames.push_back(sf::IntRect(coordX + index * width, coordY, width, height));

	m_sprite.setOrigin(width / 2, height / 2);
	m_sprite.setTextureRect(m_frames[0]);
}

void Animation::SetVariables(float coordX, float coordY, uint8_t numFrames, float speed, float width, float height, sf::Texture texture)
{
	m_frame = 0;
	m_speed = speed;

	for (int index = 0; index < numFrames; index++)
		m_frames.push_back(sf::IntRect(coordX + index * width, coordY, width, height));

	m_sprite.setOrigin(width / 2, height / 2);
	m_sprite.setTextureRect(m_frames[0]);
}
 

void Animation::SetTexture(sf::Texture texture)
{
	m_sprite.setTexture(texture);
}


void Animation::update()
{
	m_frame += m_speed;
	if (m_frame >= m_frames.size()) {
		m_frame -= m_frames.size();
	}
		
	if (m_frames.size() > 0) {
		m_sprite.setTextureRect(m_frames[int(m_frame)]);
	}
}

bool Animation::isEnd()
{
	return m_frame + m_speed >= m_frames.size();
}

sf::Sprite Animation::GetSprite()
{
	return m_sprite;
}






