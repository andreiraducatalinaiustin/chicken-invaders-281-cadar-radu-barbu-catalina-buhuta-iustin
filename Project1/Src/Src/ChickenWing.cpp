#include "..\Include\ChickenWing.h"

ChickenWing::ChickenWing(float coordx, float coordy)
{
	m_name = ObjectName::CHICKENWING;
	m_health = 1;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("ChickenWingTexturePath", ChickenWingTexturePath);
	if (!m_texture.loadFromFile(ChickenWingTexturePath))
		std::cerr << "Error while loading the texture of ChickenWing \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_coordx = coordx;
	m_coordy = coordy;
	sf::Vector2f wingTargetSize(WIDTH / 2, HEIGHT / 2);
	m_sprite.setScale(wingTargetSize.x / WIDTH, wingTargetSize.y / HEIGHT);
	timeBeforeDisapear = 50;
	m_dx = rand() % 5 - 2;
	m_dy = rand() % 5 + 2;
}

void ChickenWing::Update()
{
	if (m_coordy <= HEIGHT - 40)
	{
		m_coordx += m_dx;
		m_coordy += m_dy;
	}
	else if (timeBeforeDisapear == 0)
		m_health = 0;
	else
		--timeBeforeDisapear;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void ChickenWing::Draw()
{
}
