#include "..\Include\Chicken.h"

Chicken::Chicken()
{
	m_name = ObjectName::CHICKEN;
	m_state = ChickenState::STAY;
	
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("ChickenTexturePath", ChickenTexturePath);
	if (!m_texture.loadFromFile(ChickenTexturePath))
		std::cerr << "Error while loading texture for the Chicken \n";

	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_sprite.setTextureRect(sf::IntRect(0, 0, 140, 115));

	m_textureX = 0;
	m_textureY = 0;

	m_coordx = rand() % WIDTH;
	m_coordy = rand() % HEIGHT;
	m_health = 1;

	m_giftChance = rand() % 10;
	m_timeBeforeNewEgg = rand() % 250 + 100;
	m_timeCountdown = m_timeBeforeNewEgg;

	m_timeBeforeChangeDir = rand() % 250 + 100;
	srand(time(NULL));

}

Chicken::Chicken(float coordx, float coordy, float dx, float dy, float radius, float angle, float health, int giftChance, ChickenState state)
	: Object(coordx, coordy, dx, dy, radius, angle, health),
	m_giftChance(giftChance), m_state(state)
{
	m_name = ObjectName::CHICKEN;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("ChickenTexturePath", ChickenTexturePath);
	if (!m_texture.loadFromFile(ChickenTexturePath))
		std::cerr << "Error while loading texture for the Chicken \n";
	m_timeBeforeNewEgg = rand() % 250 + 100;
	m_timeCountdown = m_timeBeforeNewEgg;

	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_sprite.setTextureRect(sf::IntRect(0, 0, 140, 115));

	m_textureX = 0;
	m_textureY = 0;

	m_angle = angle;


}

void Chicken::Update()
{
	
	if (m_textureX == 69.5 * 8)
		if (m_textureY == 57.5 * 1)
		{
			m_textureX = 0;
			m_textureY = 0;
		}
		else {
			m_textureX = 0;
			m_textureY += 57.5;
		}
	else
		m_textureX += 69.5;
	m_sprite.setTextureRect(sf::IntRect(m_textureX, m_textureY, 69.5, 57.5));	
	
	m_dx = cos(m_angle * RADIAN)*2 ;
	m_dy = sin(m_angle * RADIAN)*2 ;

	m_coordx += m_dx;
	m_coordy += m_dy;


	m_timeBeforeChangeDir--;
	--m_timeCountdown;
	m_sprite.setPosition(m_coordx, m_coordy);
	

}


void Chicken::SetChickenDirection(const std::pair<int,int> & direction)
{
	m_direction = direction;
}

int Chicken::GetTimeBeforeNewEgg()
{
	return m_timeCountdown;
}

int Chicken::GetGiftChance()
{
	return m_giftChance;
}

int Chicken::GetTime()
{
	return m_timeBeforeNewEgg;
}

int Chicken::GetTimeBeforeNewDirection()
{
	return m_timeBeforeChangeDir;
}

ChickenState Chicken::GetState()
{
	return m_state;
}

void Chicken::SetTimeBeforeNewEgg(const int& time)
{
	m_timeBeforeNewEgg = time;
	m_timeCountdown = time;
}

void Chicken::SetTimeBeforeNewDirection(const int& time)
{
	m_timeBeforeChangeDir = time;
}

void Chicken::ResetDirection()
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_real_distribution<double> distX(140, WIDTH - 140);
	std::uniform_real_distribution<double> distY(118, HEIGHT/2);

	std::pair<int, int>newDirection;
	newDirection.first = distX(rd); newDirection.second = distY(rd);
	m_direction = newDirection;

}

Chicken::LimitReached Chicken::IsAtLimit()
{
	/*if (m_coordx >= WIDTH - 140
		&& m_coordy <= 10)
		return LimitReached::UpperRight;
	
	if (m_coordx <= 10
		&& m_coordy >= HEIGHT / 2 + 20)
		return LimitReached::LowerLeft;

	if (m_coordy <= 10 && m_coordx <= 10)
		return LimitReached::UpperLeft;

	if (m_coordy >= HEIGHT / 2 + 20 && m_coordx >= WIDTH - 140)
		return LimitReached::LowerLeft;*/

	if (m_coordx >= WIDTH - 140)
		return LimitReached::RIGHT;
	if (m_coordy >= HEIGHT / 2 + 20)
		return LimitReached::DOWN;
	if (m_coordx <= 10) {
		return LimitReached::LEFT;
	}
	if (m_coordy <= 10)
		return LimitReached::UP;
	if (((int)m_coordx == m_direction.first && (int)m_coordy == m_direction.second))
		return LimitReached::SET;


	return LimitReached::NONE;
	}

Egg* Chicken::MakeNewEgg()
{
	return new Egg(m_coordx + 70, m_coordy + 100); 
}

Gift* Chicken::MakeGift()
{
	return new Gift(m_coordx + 70 ,m_coordy + 50);
}



