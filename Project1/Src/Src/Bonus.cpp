#include "..\Include\Bonus.h"

Bonus::Bonus(int levelNumber)
{
	m_name = ObjectName::BONUS;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("BonusTexturePath", BonusTexturePath);
	if (!m_texture.loadFromFile(BonusTexturePath))
		std::cerr << "Error while loading the texture of Bonus \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_coordx = WIDTH/2;
	m_coordy = -100;
	/*m_dx = 1 + levelNumber/2;*/
	m_health = 1 + 2 * levelNumber;
	m_sprite.setPosition(m_coordx, m_coordy);
	m_targetX = 0;
	m_targetY = 0;
}

void Bonus::Update()
{
	/*m_coordx += m_dx;
	if (m_coordx > WIDTH)
		m_health = 0;*/
	if (timeBeforeNewAttack == 0)
		ResetTimeBeforeNewAttack();
	m_dx = (m_targetX - m_coordx) / WIDTH;
	m_dy = (m_targetY - m_coordy) / HEIGHT;
	if (m_dx < 0)
		m_dx += -1.5;
	else
		m_dx += 1.5;
	if (m_dy < 0)
		m_dy += -1.5;
	else
		m_dy += 1.5;
	m_coordx += m_dx;
	m_coordy += m_dy;
	--timeBeforeNewAttack;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void Bonus::Draw()
{
}

Gift* Bonus::MakeGift()
{
	Gift* gift = new Gift(m_coordx + 77, m_coordy + 62 );
	return gift;
}

ShadowBall* Bonus::MakeShadowBall(float dx, float dy)
{
	ShadowBall* shadowBall = new ShadowBall(m_coordx + 77, m_coordy + 62, dx, dy);
	return shadowBall;
}

void Bonus::setTargetLocation(int targetX, int targetY)
{
	m_targetX = targetX;
	m_targetY = targetY;
}

void Bonus::ResetTimeBeforeNewAttack()
{
	timeBeforeNewAttack = 100;
}

int Bonus::GetTimeBeforeNewAttack()
{
	return timeBeforeNewAttack;
}
