#pragma once
#include "Object.h"
class SingleRedBullet : public Object
{
public:
	SingleRedBullet();
	void Update();
	void Draw();
	void SetRotatedRightTexture();
	void SetRotatedLeftTexture();
private:
	std::string SingleRedBulletPath, SingleRedBulletRotatedLeftPath, SingleRedBulletRotatedRightPath;
	sf::Texture m_textureRotatedRight, m_textureRotatedLeft;
};
