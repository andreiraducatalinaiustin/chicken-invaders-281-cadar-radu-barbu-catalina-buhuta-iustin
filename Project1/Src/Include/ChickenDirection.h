#pragma once
#include<iostream>

enum class ChickenDirection : uint8_t
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
	NONE
};