#pragma once
#include <iostream>
#include <string>
#ifdef LOGGING_EXPORTS
#define LOGGING_API _declspec(dllexport)
#else
#define LOGGING_API _declspec(dllimport)
#endif


class LOGGING_API Logger
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error,
		PlayerName,
		Score
	};

public:
	Logger(std::ostream& os, Level minimumLevel = Level::Info);

	void log(const char * message, Level level);
	void log(const std::string& message, Level level);
	void log(const std::string& message);
	void log(const char* message);

	void setMinimumLogLevel(Level level);

private:
	std::ostream& os;
	Level minimumLevel;
};