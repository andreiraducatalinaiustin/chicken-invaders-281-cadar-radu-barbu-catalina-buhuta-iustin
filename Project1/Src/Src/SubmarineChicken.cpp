#include "..\Include\SubmarineChicken.h"

SubmarineChicken::SubmarineChicken(ChickenDirection direction)
{
	m_name = ObjectName::SUBMARINECHICKEN;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_direction = direction;
	if (m_direction == ChickenDirection::LEFT) {
		m_gameConfig->get("SubmarineChickenLeftTexturePath", SubmarineChickenTexturePath);
		m_coordx = -206 - rand() % 400;
		m_dx = rand() % 2 + 1;
	}
	else {
		m_gameConfig->get("SubmarineChickenRightTexturePath", SubmarineChickenTexturePath);
		m_coordx = WIDTH + rand() % 400;
		m_dx = -1 - rand() % 2;
	}
	if (!m_texture.loadFromFile(SubmarineChickenTexturePath))
		std::cerr << "Error while loading the texture of SubmarineChicken \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	sf::Vector2f submarineTargetSize(WIDTH / 2, HEIGHT / 2);
	m_sprite.setScale(submarineTargetSize.x / WIDTH, submarineTargetSize.y / HEIGHT);
	m_health = 1;
	m_coordy = rand() % (HEIGHT - 50);
	giftDropChance = rand() % 10;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void SubmarineChicken::Update()
{
	if (m_direction == ChickenDirection::LEFT) {
		if (m_coordx < WIDTH)
			m_coordx += m_dx;
		else
			m_health = 0;
	}
	else {
		if (m_coordx > -206)
			m_coordx += m_dx;
		else
			m_health = 0;
	}
	m_sprite.setPosition(m_coordx, m_coordy);
}

void SubmarineChicken::Draw()
{
}

Gift* SubmarineChicken::MakeGift()
{
	Gift* gift = new Gift(m_coordx + 100, m_coordy + 60);
	return gift;
}

int SubmarineChicken::GetGiftDropChance()
{
	return giftDropChance;
}
