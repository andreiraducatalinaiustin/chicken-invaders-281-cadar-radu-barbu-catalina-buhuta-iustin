#pragma once
#include "Object.h"
class BouncingAsteroid :public Object
{
public:
	BouncingAsteroid(float coordx, float coordy);
	void Update();
	void Draw();
private:
	int m_textureX;
	int m_textureY;
	std::string AsteroidTexturePath;
};