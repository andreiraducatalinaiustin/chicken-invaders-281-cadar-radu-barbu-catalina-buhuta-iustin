#include "pch.h"
#include "Logging.h"

const char* LogLevelToString(Logger::Level level)
{
	switch (level)
	{
	case Logger::Level::Info:
		return "Info";
	case Logger::Level::Warning:
		return "Warning";
	case Logger::Level::Error:
		return "Error";
	case Logger::Level::PlayerName:
		return "PlayerName";
	case Logger::Level::Score:
		return "Score";
	default:
		return "";
	}
}

Logger::Logger(std::ostream & os, Logger::Level minimumLevel) :
	os{ os },
	minimumLevel{ minimumLevel }
{
}

void Logger::log(const char * message, Level level)
{
	if (static_cast<int>(level) < static_cast<int>(minimumLevel))
		return;

	os << "[" << LogLevelToString(level) << "] " << message << std::endl;
}

void Logger::log(const std::string & message, Level level)
{
	this->log(message.c_str(), level);
}

void Logger::log(const std::string& message)
{
	this->log(message.c_str());
}

void Logger::log(const char* message)
{
	os << message << std::endl;
}

void Logger::setMinimumLogLevel(Level level)
{
	this->minimumLevel = level;
}
