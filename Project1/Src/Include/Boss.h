#pragma once
#include <SFML/Graphics.hpp>
#include"Object.h"
#include "Egg.h"
#include "ChickenWing.h"
#include "Gift.h"
class Boss :public Object 
{
public:
	Boss();
	Boss(float coordx, float coordy, float dx, float dy, float radius, float angle, float health);
	void Update();
	void Draw();
	int GetTimeBeforeNewEgg();
	void ResetTimeBeforeNewEgg();
	Egg* MakeANewEgg();
	int GetNrChickenWings();
	ChickenWing* MakeChickenWing();
	Gift* MakeGift();
	int GetGiftDropChance();
private:
	std::string BossTexturePath;
	int timeBeforeChangeDiraction;
	int timeBeforeNewEgg;
	int nrChickenWings;
	int giftDropChance;
	float m_textureX;
	float m_textureY;
};
