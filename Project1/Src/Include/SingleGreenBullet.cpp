#include "SingleGreenBullet.h"

SingleGreenBullet::SingleGreenBullet()
{
	m_name = ObjectName::BULLET;
	m_health = 1;
	m_damage = 5;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();

	m_gameConfig->get("SingleGreenBulletPath", SingleGreenBulletPath);
	m_gameConfig->get("SingleGreenBulletMediumPath", SingleGreenBulletMediumPath);
	m_gameConfig->get("SingleGreenBulletStrongPath", SingleGreenBulletStrongPath);


	m_texture.loadFromFile(SingleGreenBulletPath);
	m_textureMedium.loadFromFile(SingleGreenBulletMediumPath);
	m_textureStrong.loadFromFile(SingleGreenBulletStrongPath);

	m_texture.setSmooth(true);
	m_textureMedium.setSmooth(true);
	m_textureStrong.setSmooth(true);

	m_sprite.setTexture(m_texture);
	m_mediumSprite.setTexture(m_textureMedium);
	m_strongSprite.setTexture(m_textureStrong);

}

void SingleGreenBullet::Update()
{
	m_dx = cos(270 * RADIAN) * 6;
	m_dy = sin(270 * RADIAN) * 6;
	m_coordx += m_dx;
	m_coordy += m_dy;

	if (m_coordx > WIDTH || m_coordx<0 || m_coordy> HEIGHT || m_coordy < 0)
		m_health = 0;

	m_sprite.setPosition(m_coordx, m_coordy);
}

void SingleGreenBullet::Draw()
{
}

void SingleGreenBullet::SetMediumDamage()
{
	m_damage = 7;
}

void SingleGreenBullet::SetMediumTexture()
{
	m_sprite = m_mediumSprite;
}

void SingleGreenBullet::SetStrongDamage()
{
	m_damage = 10;
}

void SingleGreenBullet::SetStrongTexture()
{
	m_sprite = m_strongSprite;
}
