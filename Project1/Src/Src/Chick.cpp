#include "..\Include\Chick.h"

Chick::Chick(float coordx, float coordy)
{
	m_name = ObjectName::CHICK;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("ChickTexturePath", ChickTexturePath);
	if(!m_texture.loadFromFile(ChickTexturePath))
		std::cerr << "Error while loading the texture of Chick \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_coordx = coordx;
	m_coordy = coordy;
	m_health = 1;
	//m_dx = rand() % 3 + 1;
	//m_dy = rand() % 5 - 2;
	//timeBeforeChangeDiraction = rand() % 50 + 50;
	timeBeforeNewEgg = rand() % 50 + 100;
	m_sprite.setPosition(m_coordx, m_coordy);
	m_targetX = 0;
	m_targetY = 0;
}

void Chick::Update()
{
	/*if (m_coordx <= 0)
	{
		m_dx = rand() % 2;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}
	else if (m_coordx >= WIDTH - m_sprite.getGlobalBounds().width)
	{
		m_dx = rand() % 2 - 1;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}
	else if (timeBeforeChangeDiraction == 0)
	{
		m_dx = rand() % 10 - 4;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}

	if (m_coordy <= 0)
	{
		m_dy = rand() % 2;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}
	else if (m_coordy >= HEIGHT - m_sprite.getGlobalBounds().height)
	{
		m_dy = rand() % 2 - 1;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}
	else if (timeBeforeChangeDiraction == 0)
	{
		m_dy = rand() % 3 - 1;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}
	m_coordx += m_dx;
	m_coordy += m_dy;
	--timeBeforeChangeDiraction;
	*/
	if (timeBeforeNewEgg == 0)
		ResetTimeBeforeNewEgg();
	m_dx = (m_targetX - m_coordx) / WIDTH;
	m_dy = (m_targetY - m_coordy) / HEIGHT;
	if (m_dx < 0)
		m_dx += -1.5;
	else
		m_dx += 1.5;
	if (m_dy < 0)
		m_dy += -1.5;
	else
		m_dy += 1.5;
	m_coordx += m_dx;
	m_coordy += m_dy;
	--timeBeforeNewEgg;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void Chick::Draw()
{
}

int Chick::GetTimeBeforeNewEgg()
{
	return timeBeforeNewEgg;
}

void Chick::ResetTimeBeforeNewEgg()
{
	timeBeforeNewEgg = rand() % 50 + 100;
}

Egg* Chick::MakeANewEgg()
{
	Egg* egg = new Egg(m_coordx + 15, m_coordy + 41);
	return egg;
}

Gift* Chick::MakeGift()
{
	Gift* gift = new Gift(m_coordx + 15, m_coordy + 41);
	return gift;
}

int Chick::GetGiftDropChance()
{
	giftDropChance = rand() % 10;
	return giftDropChance;
}

void Chick::setTargetLocation(int targetX, int targetY)
{
	m_targetX = targetX;
	m_targetY = targetY;
}
