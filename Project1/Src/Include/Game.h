#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>
#include <list>
#include <memory>
#include <fstream>
#include <sstream>
#include "Animation.h"
#include "Asteroid.h"
#include "Boss.h"
#include "Bullet.h"
#include "Chicken.h"
#include "ChickenState.h"
#include "IDrawable.h"
#include "Object.h"
#include "ObjectName.h"
#include "Player.h"
#include "MainMenu.h"
#include "ExplosionTypeA.h"
#include "ExplosionTypeB.h"
#include "ExplosionTypeC.h"
#include "SingleRedBullet.h"
#include "SingleGreenBullet.h"
#include "Level.h"
#include "MusicPlayer.h"
#include "..\Logging.h"
#include "..\..\Project1\ScoreWindow.h";


class Game
{
public:
	void StartGame();
	void MainMenuInitialisation();
	bool Collision(const Object* object1, const Object* object2);

	void SetObjectList(std::list<Object*> objectList);
	std::list<Object*> GetObjectList() const;

private:
	void StartGameInitialisation();
	void DrawObject();
	void ObjectsUpdate();
	void ScoreUpdate();
	void WaveStringUpdate();
	void ColisionManager();
	void ScreenRender();
	void FirePlaySound(const BulletType& bulletType);
	std::string MakeStringWaveNr(int number);

	void EndGame();
	void EndGameInitialisation();
	void EndGameScreenRender();

	void SetGameOverText();
	void SetHighScoreText();
	void SetPlayerInputText();
	void ScoreInitialisation();
	void ScoreWindowInit(ScoreWindow& scoreWindow);

	void HintsUpdate();
	void HintsInitialisation();

	std::unique_ptr<Logger> m_logger;
	std::vector<std::pair<std::string, std::string>> listOfScores;
	sf::Clock m_GhostClock;
	float m_GhostTimerUpdate = 2;
	sf::Vector2f m_ghostPosition;
	bool m_wasCalledGhostPosition = false;
	std::unique_ptr<Player> m_player;
	std::unique_ptr<MusicPlayer> m_collisionSound;
	std::unique_ptr<GameConfig> m_gameConfig;
	std::unique_ptr<MusicPlayer> m_musicPlayer;
	std::list<Object*> m_objects;
	sf::RenderWindow* window;
	Animation m_playerAnimation;
	sf::Clock m_dt_clock, m_Clock, m_ShootingClock;
	float m_dt;
	bool m_playerWasInvincible = false;
	sf::Texture m_backgroundTexture;
	sf::Sprite m_backgroundSprite;
	sf::Font m_font;
	sf::Text m_text;
	sf::Texture m_hearthTexture;
	sf::Sprite m_healthSprite;
	uint64_t m_gameScore;
	std::vector<sf::Sprite> m_hearts;
	int levelNumber = 0;
	int waveNumber = 0;
	Level* m_level;
	bool m_showWaveNr = true, m_firstWave = true;
	sf::Text m_textWaveNr, m_textGameOver , m_textHighScore;
	sf::Text m_afkHintsText, m_timeToShootText, m_pressToRestartText;
	int m_waveNr = 1;
	float m_waveNrTimeToDisplay = 2.5f;
	sf::Clock m_waveNrClock;
	sf::Clock m_afkClock;
	float m_afkTimer = 3.0f;
	bool m_isAfk = false;
	bool m_firstShotWasFired = false;
	float m_minTimeBeforeShooting = 0.2f;
	float m_scoreMultiplyer = 1.0f;
	float m_scoreMultiplyerIncreaseRatio = 0.05f;
	bool m_timeToShootBool = false;
	sf::Clock m_pressToRestartClock;
	float m_timeToShowRestartText = 1.0f;


	//HighScore Name imput
	std::string m_playerInput;
	sf::Text m_playerText;
};
