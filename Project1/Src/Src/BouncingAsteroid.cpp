#include "BouncingAsteroid.h"

BouncingAsteroid::BouncingAsteroid(float coordx, float coordy)
{
	m_name = ObjectName::ASTEROID;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("AsteroidTexturePath", AsteroidTexturePath);
	if (!m_texture.loadFromFile(AsteroidTexturePath))
		std::cerr << "Error while loading the texture of BigAsteroid \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_sprite.setTextureRect(sf::IntRect(0, 0, 40, 40));
	m_textureX = 0;
	m_textureY = 0;
	m_health = 5;
	m_coordx = coordx;
	m_coordy = coordy;
	m_dx = rand() % 11 - 5;
	m_dy = rand() % 11 - 5;
	if (m_dx == 0 && m_dy == 0)
		m_dy = 3;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void BouncingAsteroid::Update()
{
	m_coordx += m_dx;
	m_coordy += m_dy;
	if (m_coordx + 40 > WIDTH || m_coordx < 0)
		m_dx = -m_dx;
	if (m_coordy + 40 > HEIGHT || m_coordy < 0)
		m_dy = -m_dy;
	if (m_textureX == 40 * 7)
		if (m_textureY == 40 * 7)
		{
			m_textureX = 0;
			m_textureY = 0;
		}
		else {
			m_textureX = 0;
			m_textureY += 40;
		}
	else
		m_textureX += 40;
	m_sprite.setTextureRect(sf::IntRect(m_textureX, m_textureY, 40, 40));

	m_sprite.setPosition(m_coordx, m_coordy);
}

void BouncingAsteroid::Draw()
{
}
