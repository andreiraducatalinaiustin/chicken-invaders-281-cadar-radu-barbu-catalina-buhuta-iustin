#pragma once
#include <vector>
#include "Object.h"
#include "Asteroid.h"
#include "Boss.h"
#include "Chicken.h"
#include "BigEgg.h"
#include "SubmarineChicken.h"
#include "ChickenDirection.h"
#include "BouncingBigAsteroid.h"
#include "Bonus.h"

class Level {
public:
	Level(const int& levelNumber);
	std::vector<Object*> &operator[](const int& position);
	void DeleteElement(Object* obj, const int& wave_number);

	void UpdateWave1();
	void UpdateWave3(const Chicken::LimitReached& limit);
	void UpdateWave2();
	void UpdateWave5(const Chicken::LimitReached& limit);
private:
	void CreateWave2(const int& levelNumber);
	void CreateWave3(const int& levelNumber);
	void CreateWave5(const int& levelNumber);
	
private:
	std::vector<std::vector<Object*>> m_waves;
	std::vector<Object*>m_wave1;
	std::vector<Object*>m_wave3;
	std::vector<Object*>m_wave2;
	std::vector<Object*>m_wave5;
};