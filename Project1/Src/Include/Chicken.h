#pragma once
#include <SFML/Graphics.hpp>
#include"Object.h"
#include "Egg.h"
#include "Gift.h"
#include <random>
#include "ChickenState.h"
class Chicken : public Object
{
public:
	enum LimitReached : uint8_t
	{
		RIGHT,
		LEFT,
		UP,
		DOWN,
		SET,
		UpperRight,
		UpperLeft,
		LowerRight,
		LowerLeft,
		NONE
	};

public:
	Chicken();
	Chicken(float coordx, float coordy, float dx, float dy,
		float radius, float angle, float health, int giftChance, ChickenState state);
	void Update();
	LimitReached IsAtLimit();
	Egg* MakeNewEgg();
	Gift* MakeGift();

public:
	void SetChickenDirection(const std::pair<int, int>& direction);
	int GetTimeBeforeNewEgg();
	int GetGiftChance();
	int GetTime();
	int GetTimeBeforeNewDirection();
	ChickenState GetState();
	void SetTimeBeforeNewEgg(const int & time);
	void SetTimeBeforeNewDirection(const int& time);
	void ResetDirection();

private:
	std::string ChickenTexturePath;
	std::pair<int,int> m_direction;
	float m_textureX;
	float m_textureY;
	int m_timeBeforeNewEgg;
	int m_giftChance;
	float m_speed;
	int m_frame;
	int m_timeCountdown;
	int m_timeBeforeChangeDir;
	ChickenState m_state;
	
	




};
