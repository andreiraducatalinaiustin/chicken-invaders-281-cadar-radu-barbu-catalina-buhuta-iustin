#pragma once
#include "Object.h"
#include "Egg.h"
#include "Gift.h"
class Chick :public Object
{
public:
	Chick(float coordx, float coordy);
	void Update();
	void Draw();
	int GetTimeBeforeNewEgg();
	void ResetTimeBeforeNewEgg();
	Egg* MakeANewEgg();
	Gift* MakeGift();
	int GetGiftDropChance();
	void setTargetLocation(int targetX, int targetY);
private:
	std::string ChickTexturePath;
	int timeBeforeChangeDiraction;
	int timeBeforeNewEgg;
	int giftDropChance;
	int m_targetX;
	int m_targetY;
};