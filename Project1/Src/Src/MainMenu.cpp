#include "../Include/MainMenu.h"

MainMenu::MainMenu(float width, float heigth)
{
	m_gameConfig = std::make_unique<GameConfig>();

	m_gameConfig->get("BackgroundTexturePath", BackgroundTexturePath);
	m_gameConfig->get("LogoTexturePath", LogoTexturePath);
	m_gameConfig->get("StartButtonTexturePath", StartButtonTexturePath);
	m_gameConfig->get("QuitButtonTexturePath", QuitButtonTexturePath);
	m_gameConfig->get("QuitButtonHoverTexturePath", QuitButtonHoverTexturePath);
	m_gameConfig->get("StartButtonHoverTexturePath", StartButtonHoverTexturePath);
	m_gameConfig->get("ScoresTexturePath", ScoresTexturePath);


	if (!m_backgroundTexture.loadFromFile(BackgroundTexturePath))
		std::cerr << "Error while loading the texture of Background \n";

	if (!m_logoTexture.loadFromFile(LogoTexturePath))
		std::cerr << "Error while loading the texture of Logo \n";

	if (!m_startButtonTexture.loadFromFile(StartButtonTexturePath))
		std::cerr << "Error while loading the texture of Start Button";

	if (!m_quitButtonTexture.loadFromFile(QuitButtonTexturePath))
		std::cerr << "Error while loading the texture of Quit Button";

	if (!m_quitButtonHoverTexture.loadFromFile(QuitButtonHoverTexturePath))
		std::cerr << "Error while loading the texture of Quit Button Hover";

	if (!m_startButtonHoverTexture.loadFromFile(StartButtonHoverTexturePath))
		std::cerr << "Error while loading the texture of start Button Hover";

	if (!m_scoresTexture.loadFromFile(ScoresTexturePath))
		std::cerr << "Error while loading the texture of score Button";

	m_logoTexture.setSmooth(true);
	m_backgroundTexture.setSmooth(true);
	m_startButtonTexture.setSmooth(true);
	m_quitButtonTexture.setSmooth(true);
	m_quitButtonHoverTexture.setSmooth(true);
	m_startButtonHoverTexture.setSmooth(true);
	m_scoresTexture.setSmooth(true);

	m_backgroundImage.setTexture(m_backgroundTexture);
	m_logoImage.setTexture(m_logoTexture);
	m_startButtonImage.setTexture(m_startButtonTexture);
	m_quitButtonImage.setTexture(m_quitButtonTexture);
	m_scoresButton.setTexture(m_scoresTexture);

	sf::Vector2f backgrTargetSize(width, heigth);
	sf::Vector2f logoTargetSize(width / 2, heigth / 3);
	sf::Vector2f startButtonTargetSize(width / 6, heigth / 8);
	

	m_backgroundImage.setScale(backgrTargetSize.x / m_backgroundImage.getLocalBounds().width,
		backgrTargetSize.y / m_backgroundImage.getLocalBounds().height);
	m_logoImage.setScale(logoTargetSize.x / m_logoImage.getLocalBounds().width,
		logoTargetSize.x / m_logoImage.getLocalBounds().height);
	m_startButtonImage.setScale(startButtonTargetSize.x / m_startButtonImage.getLocalBounds().width * 1.5,
		startButtonTargetSize.y / m_startButtonImage.getLocalBounds().height);
	m_quitButtonImage.setScale(startButtonTargetSize.x / m_startButtonImage.getLocalBounds().width * 1.5,
		startButtonTargetSize.y / m_startButtonImage.getLocalBounds().height);
	m_scoresButton.setScale(startButtonTargetSize.x / m_startButtonImage.getLocalBounds().width * 1.5,
		startButtonTargetSize.y / m_startButtonImage.getLocalBounds().height);

	m_logoImage.setPosition(sf::Vector2f(width / 4, -heigth / 2.5));
	m_startButtonImage.setPosition(sf::Vector2f(width / 2.75, m_logoImage.getPosition().y + m_logoImage.getGlobalBounds().height));
	m_quitButtonImage.setPosition(sf::Vector2f(m_startButtonImage.getPosition().x, m_startButtonImage.getPosition().y * 1.25));
	m_scoresButton.setPosition(sf::Vector2f(m_quitButtonImage.getPosition().x, m_quitButtonImage.getPosition().y * 1.25));



}



void MainMenu::StartMenu(sf::RenderWindow* window)
{

	window->setFramerateLimit(60);
	window->clear();

	window->draw(this->m_backgroundImage);
	window->draw(this->m_logoImage);

	window->draw(this->m_startButtonImage);
	window->draw(this->m_quitButtonImage);
	window->draw(this->m_scoresButton);
	window->display();

}

void MainMenu::HoverStartButton()
{
	m_startButtonImage.setTexture(m_startButtonHoverTexture);
}

void MainMenu::HoverQuitButton()
{
	m_quitButtonImage.setTexture(m_quitButtonHoverTexture);
}

void MainMenu::ReverseQuitButton()
{
	m_quitButtonImage.setTexture(m_quitButtonTexture);
}

void MainMenu::ReverseStartButton()
{
	m_startButtonImage.setTexture(m_startButtonTexture);
}


sf::Sprite MainMenu::GetStartButtonSprite()
{
	return this->m_startButtonImage;
}

sf::Sprite MainMenu::GetQuitButtonSprite()
{
	return this->m_quitButtonImage;
}

sf::Sprite MainMenu::GetScoresButtonSprite()
{
	return m_scoresButton;
}


MainMenu::~MainMenu()
{
}
