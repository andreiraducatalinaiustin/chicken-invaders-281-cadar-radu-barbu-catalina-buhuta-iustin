#include "..\Include\ShadowBall.h"

ShadowBall::ShadowBall(float coordx, float coordy, float dx, float dy)
{
	m_name = ObjectName::SHADOWBALL;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("ShadowBallTexturePath", ShadowBallTexturePath);
	if (!m_texture.loadFromFile(ShadowBallTexturePath))
		std::cerr << "Error while loading the texture of ShadowBall \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_coordx = coordx;
	m_coordy = coordy;
	m_dx = dx;
	m_dy = dy;
	m_health = 1;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void ShadowBall::Update()
{
	m_coordx += m_dx;
	m_coordy += m_dy;
	if (m_coordx<-41 || m_coordx>WIDTH || m_coordy<-36 || m_coordy>HEIGHT)
		m_health = 0;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void ShadowBall::Draw()
{
}
