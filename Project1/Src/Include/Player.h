#pragma once
#include <SFML/Graphics.hpp>
#include"Object.h"
#include "Bullet.h"
#include "SingleRedBullet.h"
#include "SingleGreenBullet.h"
#include "BulletType.h"

class Player : public Object
{
public:
	Player();
	void Fire(std::list<Object*> &objects);
	void SetThrust(bool thrust);
	bool GetThrust();
	bool GetInvincible();
	uint16_t GetInvincibleDuration();

	void incrementBulletLevel(BulletType bulletType);

	BulletType getBulletType();

	uint8_t getBlueBulletLevel();
	uint8_t getRedBulletLevel();
	uint8_t getGreenBulletLevel();
	uint8_t getMaxBlueBulletLevel();
	uint8_t getMaxRedBulletLevel();
	uint8_t getMaxGreenBulletLevel();


	sf::Clock& GetClock();
	void Update();
	void Draw();
	
	void SetInvincible(bool invincible);
	void SetSpriteDefault();
	void SetSpriteLeft();
	void SetSpriteRight();

	void SetSpriteUpFlames();
	void SetSpriteLeftFlames();
	void SetSpriteRightFlames();

	void SetAngle(uint16_t angle);
	void SetBulletType(BulletType bulletType);

	sf::Vector2f getVelocity()const;
	void setVelocity(sf::Vector2f velocity);
	const float getMovementSpeed()const;
	void CoordonateSettings(float coordx, float coordy);
	sf::Sprite getSpriteInvincible();

private:

	bool m_thrust;
	bool m_isInvincible = false;
	sf::Vector2f m_velocity;
	const float m_movementSpeed = 400.f;

	uint16_t m_playerAngle = 0, m_invincibleDuration;
	sf::Sprite m_spritePlayer, m_spritePlayerLeft, m_spritePlayerRight,
			   m_spritePlayerFlames, m_spritePlayerUpFlames, m_spritePlayerLeftFlames, m_spritePlayerRightFlames;
	sf::Texture PlayerTexture, PlayerTextureLeft, PlayerTextureRight,
				PlayerTextureFlames, PlayerTextureUpFlames, PlayerTextureLeftFlames, PlayerTextureRightFlames;
	std::string PlayerTexturePath, PlayerTextureLeftPath, PlayerTextureRightPath,
		PlayerTextureFlamesPath, PlayerTextureUpFlamesPath, PlayerTextureLeftFlamesPath, PlayerTextureRightFlamesPath;
	sf::Clock m_clock;
	sf::Texture InvincibleTexture;
	sf::Sprite m_spriteInvincible;
	std::string InvincibleTexturePath;
	BulletType m_bulletType;
	uint8_t m_blueBulletLevel, m_redBulletLevel, m_greenBulletLevel;
	uint8_t m_maxBlueBulletLevel, m_maxRedBulletLevel, m_maxGreenBulletLevel;
};