#include "..\Include\Gift.h"

Gift::Gift(float coordx, float coordy)
{
	giftType = rand() % 3;
	m_health = 1;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	switch (giftType)
	{
	  case 0:
	  {
		  m_name = ObjectName::BLUEGIFT;
		  m_gameConfig->get("BlueGiftTexturePath", GiftTexturePath);
	  }
	  break;
	  case 1:
	  {
		  m_name = ObjectName::REDGIFT;
		  m_gameConfig->get("RedGiftTexturePath", GiftTexturePath);
	  }
	  break;
	  case 2:
	  {
		  m_name = ObjectName::GREENGIFT;
		  m_gameConfig->get("GreenGiftTexturePath", GiftTexturePath);
	  }
	  break;

	}
	
	if (!m_texture.loadFromFile(GiftTexturePath))
		std::cerr << "Error while loading the texture of Gift \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_coordx = coordx;
	m_coordy = coordy;
	sf::Vector2f wingTargetSize(WIDTH / 2, HEIGHT / 2);
	m_sprite.setScale(wingTargetSize.x / WIDTH, wingTargetSize.y / HEIGHT);
	m_dy = rand() % 2 + 2;
}

void Gift::Update()
{
	if (m_coordy >= HEIGHT || m_coordx >= WIDTH)
		m_health = 0;
	else
		m_coordy += m_dy;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void Gift::Draw()
{
}
