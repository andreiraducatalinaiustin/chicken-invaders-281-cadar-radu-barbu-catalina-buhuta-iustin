#pragma once
#include <iostream>

enum class BulletType : uint8_t
{
	BLUEBULLET,
	GREENBULLET,
	REDBULLET
};