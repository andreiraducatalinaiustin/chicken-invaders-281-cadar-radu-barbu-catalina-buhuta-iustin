#pragma once
#include <iostream>

enum class ChickenState : uint8_t
{
	STAY,
	GROUPED,
	FALL,
	RANDOM
};
