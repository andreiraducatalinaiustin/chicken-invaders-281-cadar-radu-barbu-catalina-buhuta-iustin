#include "Game.h"
#include <ChickenDeath.h>
#include<random>


void Game::StartGame()
{

	StartGameInitialisation();
	
	while (this->window->isOpen())
	{
		
		m_dt = m_dt_clock.restart().asSeconds();
		sf::Event event;
		while (this->window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				this->window->close();
			
			if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::Space)
				{
					if (!m_firstShotWasFired)
					{
						m_player->Fire(m_objects);
						FirePlaySound(m_player->getBulletType());
						m_firstShotWasFired = true;
						m_ShootingClock.restart();
					}
					else
						if (m_ShootingClock.getElapsedTime().asSeconds() > m_minTimeBeforeShooting)
						{
							m_player->Fire(m_objects);
							FirePlaySound(m_player->getBulletType());
							m_ShootingClock.restart();
						}
					m_afkClock.restart();
				}
			}
		}

		sf::Vector2f currentVelocity(0.f, 0.f);
		m_player->setVelocity(currentVelocity);


		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			m_player->SetAngle(270);
			m_player->SetThrust(true);
			currentVelocity.y += -m_player->getMovementSpeed() * m_dt;

			m_player->SetSpriteUpFlames();
			m_afkClock.restart();
		}
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			m_player->SetAngle(90);
			m_player->SetThrust(true);
			currentVelocity.y += m_player->getMovementSpeed() * m_dt;
			m_player->SetSpriteDefault();
			m_afkClock.restart();

		}
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			m_player->SetAngle(180);
			m_player->SetThrust(true);
			currentVelocity.x += -m_player->getMovementSpeed() * m_dt;
			m_player->SetSpriteLeftFlames();
			m_afkClock.restart();
		}
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			m_player->SetAngle(0);
			m_player->SetThrust(true);
			currentVelocity.x += m_player->getMovementSpeed() * m_dt;
			m_player->SetSpriteRightFlames();
			m_afkClock.restart();
		}

		if(currentVelocity.x || currentVelocity.y)
			m_player->setVelocity(currentVelocity);

		else
		{
			m_player->SetSpriteDefault();
			m_player->SetThrust(false);
		}

		ColisionManager();
		ObjectsUpdate();

		ScoreUpdate();
		HintsUpdate();

		ScreenRender();
	}
}

void Game::ScoreWindowInit(ScoreWindow& scoreWindow) {

	while (window->isOpen()) {
		sf::Event event;

		while (window->pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window->close();
		}
		window->clear();
		scoreWindow.SetScores(listOfScores, WIDTH, HEIGHT);
		scoreWindow.StartScoreWindow(window);
		
	}

}

void Game::MainMenuInitialisation()
{
	this->window = new sf::RenderWindow(sf::VideoMode(WIDTH, HEIGHT), "Chicken Invaders");
	this->window->setFramerateLimit(60);
	MainMenu menu(WIDTH, HEIGHT);
	bool isClicked = false;
	bool quitHover = false;
	bool startHover = false;
	
	ScoreWindow scoreWindow(WIDTH, HEIGHT);

	m_musicPlayer = std::make_unique<MusicPlayer>(m_gameConfig);
	m_musicPlayer->PlayMainTheme();

	std::ofstream of("scores.txt", std::ios::app);
	m_logger = std::make_unique<Logger>(of);
	
	while (window->isOpen() && !isClicked) {
		sf::Event event;

		while (window->pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window->close();
		}
		auto pos = sf::Mouse::getPosition(*window);
		auto mouse_pos = window->mapPixelToCoords(pos);

		if (event.type == sf::Event::MouseButtonReleased) {

			if (menu.GetStartButtonSprite().getGlobalBounds().contains(mouse_pos)) {
				isClicked = true;
				this->StartGame();
			}

			if (menu.GetQuitButtonSprite().getGlobalBounds().contains(mouse_pos)) {
				isClicked = true;
				window->close();
			}
			if (menu.GetScoresButtonSprite().getGlobalBounds().contains(mouse_pos)) {
				isClicked = true;
				window->clear();
				ScoreInitialisation();
				ScoreWindowInit(scoreWindow);
				

				
			}
		}
		if (!quitHover && menu.GetQuitButtonSprite().getGlobalBounds().contains(mouse_pos)) {
			menu.HoverQuitButton();
			quitHover = true;
		}
		if (quitHover && !menu.GetQuitButtonSprite().getGlobalBounds().contains(mouse_pos)) {
			menu.ReverseQuitButton();
			quitHover = false;
		}
		if (!startHover && menu.GetStartButtonSprite().getGlobalBounds().contains(mouse_pos)) {
			menu.HoverStartButton();
			startHover = true;
		}
		if (startHover && !menu.GetStartButtonSprite().getGlobalBounds().contains(mouse_pos)) {
			menu.ReverseStartButton();
			startHover = false;

		}



		menu.StartMenu(window);
	}
}

bool Game::Collision(const Object* object1, const Object* object2)
{
     return (object2->GetCoordX() - object1->GetCoordX()) * (object2->GetCoordX() - object1->GetCoordX()) +
			(object2->GetCoordY() - object1->GetCoordY()) * (object2->GetCoordY() - object1->GetCoordY()) <
			(object1->GetRadius() + object2->GetRadius()) * (object1->GetRadius() + object2->GetRadius());
}

void Game::StartGameInitialisation()
{
	m_gameScore = 0;

	m_musicPlayer->StopMainTheme();


	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();	
	
	std::string BackgroundTextureString;
	m_gameConfig->get("BackgroundTextureString", BackgroundTextureString);

	m_backgroundTexture.loadFromFile(BackgroundTextureString);
	m_backgroundSprite.setTexture(m_backgroundTexture);
	
	sf::Vector2f windowSize = window->getView().getSize();
	m_backgroundSprite.setScale(
		windowSize.x / m_backgroundSprite.getLocalBounds().width,
		windowSize.y / m_backgroundSprite.getLocalBounds().height);

	std::string font;
	m_gameConfig->get("Font", font);

	m_font.loadFromFile(font);
	m_text.setFont(m_font);
	m_text.setFillColor(sf::Color::White);

	m_textWaveNr.setFont(m_font);
	m_textWaveNr.setFillColor(sf::Color::White);
	WaveStringUpdate();
	m_textWaveNr.setOutlineColor(sf::Color::Blue);
	m_textWaveNr.setOutlineThickness(5.f);
	m_textWaveNr.setCharacterSize(60);
	m_textWaveNr.setStyle(sf::Text::Bold);
	m_textWaveNr.setLetterSpacing(2.f);
	m_textWaveNr.setPosition(WIDTH / 2 - m_textWaveNr.getGlobalBounds().width/2,
							HEIGHT / 2 - m_textWaveNr.getGlobalBounds().height/2);

	HintsInitialisation();
	

	std::string hearthTexturePath;
	m_gameConfig->get("HearthTexturePath", hearthTexturePath);

	m_hearthTexture.loadFromFile(hearthTexturePath);
	m_hearthTexture.setSmooth(true);
	m_healthSprite.setTexture(m_hearthTexture);

	for (int index = 0; index < 3; ++index)
		m_hearts.push_back(m_healthSprite);

	m_gameScore = 0;

	m_player = std::make_unique<Player>();

	m_level = new Level(levelNumber++);

	m_objects.push_back(m_player.get());
	std::vector<Object*> wave = m_level->operator[](waveNumber++);
	for (int index2 = 0; index2 < wave.size(); ++index2)
			m_objects.push_back(wave[index2]);


	 m_afkClock.restart();
}

void Game::DrawObject()
{
	for (const auto& object : m_objects)
	{
		window->draw(object->GetSprite());
		if (object->GetObjectName() == ObjectName::PLAYER)
		{
			Player* player = dynamic_cast<Player*>(object);
			if (player->GetInvincible())
				window->draw(player->getSpriteInvincible());

			for (int index = 0; index < player->GetHealth(); ++index)
			{
				m_hearts[index].setPosition(m_text.getLocalBounds().width +
					m_healthSprite.getLocalBounds().width * index + 5, 0);
				window->draw(m_hearts[index]);
			}
		}

	}
}

void Game::ObjectsUpdate()
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_real_distribution<double> distX(140, WIDTH - 140);
	std::uniform_real_distribution<double> distY(118, HEIGHT / 2);
	auto it = m_objects.begin();
	bool existElementsFromWave = false;
	Chicken::LimitReached changeDirection = Chicken::LimitReached::NONE;
	while (it != m_objects.end())
	{
		Object* object = *it;
		if (!existElementsFromWave && object->GetObjectName() != ObjectName::PLAYER && object->GetObjectName() != ObjectName::BULLET && object->GetObjectName() != ObjectName::GHOSTPLAYER)
			existElementsFromWave = true;
		if (object->GetObjectName()==ObjectName::BOSS && static_cast<Boss*>(object)->GetTimeBeforeNewEgg() == 0)
			m_objects.push_back(static_cast<Boss*>(object)->MakeANewEgg());
		if (object->GetObjectName() == ObjectName::CHICKEN) {
			Chicken* chick = static_cast<Chicken*>(object);
			if (chick->GetTimeBeforeNewEgg() == 0) {
				m_objects.push_back(chick->MakeNewEgg());
				chick->SetTimeBeforeNewEgg(chick->GetTime());
			}
				
		}
		if (object->GetObjectName() == ObjectName::CHICK )
		{
			if(static_cast<Chick*>(object)->GetTimeBeforeNewEgg() == 0)
				m_objects.push_back(static_cast<Chick*>(object)->MakeANewEgg());
			static_cast<Chick*>(object)->setTargetLocation(m_player->GetCoordX(), m_player->GetCoordY());
		}
		if (object->GetObjectName() == ObjectName::BONUS &&
			static_cast<Bonus*>(object)->GetTimeBeforeNewAttack() == 0)
		{
			int dx = rand() % 10;
			int dy = 9-dx;
			m_objects.push_back(static_cast<Bonus*>(object)->MakeShadowBall(dx, dy));
			m_objects.push_back(static_cast<Bonus*>(object)->MakeShadowBall(-dy, dx));
			m_objects.push_back(static_cast<Bonus*>(object)->MakeShadowBall(dy, -dx));
			m_objects.push_back(static_cast<Bonus*>(object)->MakeShadowBall(-dx, -dy));
		}
		if (object->GetObjectName() == ObjectName::BONUS)
			static_cast<Bonus*>(object)->setTargetLocation(m_player->GetCoordX(), m_player->GetCoordY());
		if (object->GetHealth() < 1)
		{
			if (object->GetObjectName() == ObjectName::BOSS)
			{
				if(static_cast<Boss*>(object)->GetGiftDropChance()==1)
					m_objects.push_back(static_cast<Boss*>(object)->MakeGift());
				for (int index = 0; index < static_cast<Boss*>(object)->GetNrChickenWings(); ++index)
					m_objects.push_back(static_cast<Boss*>(object)->MakeChickenWing());
			}
			if (object->GetObjectName() == ObjectName::ASTEROID)
			{
				ExplosionTypeA* explosion = new ExplosionTypeA(object->GetCoordX() + object->GetSprite().getGlobalBounds().width / 2,
					object->GetCoordY() + object->GetSprite().getGlobalBounds().height / 2);
				m_objects.push_back(explosion);

			}
			if (object->GetObjectName() == ObjectName::CHICKEN)
			{
				if (static_cast<Chicken*>(object)->GetGiftChance() == 1)
					m_objects.push_back(static_cast<Chicken*>(object)->MakeGift());
			}
			if (object->GetObjectName() == ObjectName::BIGEGG)
			{
				if (static_cast<BigEgg*>(object)->GetCoordY() < HEIGHT)
					m_objects.push_back(static_cast<BigEgg*>(object)->MakeChick(static_cast<BigEgg*>(object)->GetCoordX(),
						static_cast<BigEgg*>(object)->GetCoordY(), levelNumber));
			}
			if (object->GetObjectName() == ObjectName::CHICK)
			{
				if (static_cast<Chick*>(object)->GetGiftDropChance() == 1)
					m_objects.push_back(static_cast<Chick*>(object)->MakeGift());
			}
			if (object->GetObjectName() == ObjectName::SUBMARINECHICKEN)
			{
				if (static_cast<SubmarineChicken*>(object)->GetGiftDropChance() == 1)
					m_objects.push_back(static_cast<SubmarineChicken*>(object)->MakeGift());
			}
			if (object->GetObjectName() == ObjectName::BOUNCINGBIGASTEROID)
			{
				ExplosionTypeA* explosion = new ExplosionTypeA(object->GetCoordX() + object->GetSprite().getGlobalBounds().width / 2,
					object->GetCoordY() + object->GetSprite().getGlobalBounds().height / 2);
				if (waveNumber == 8)
				{
					for (int index = 0; index < 3; index++)
						m_objects.push_back(static_cast<BouncingBigAsteroid*>(object)->MakeSmallAsteroid(
							static_cast<BouncingBigAsteroid*>(object)->GetCoordX(), static_cast<BouncingBigAsteroid*>(object)->GetCoordY(), levelNumber));
				}
				m_objects.push_back(explosion);

			}
			if (object->GetObjectName() == ObjectName::BONUS)
			{
				m_objects.push_back(static_cast<Bonus*>(object)->MakeGift());
			}
			it = m_objects.erase(it);
			m_level->DeleteElement(object, waveNumber);
		}
		else
		{
			if (object->GetObjectName() == ObjectName::CHICKEN) {
				Chicken* chicken = dynamic_cast<Chicken*>(object);
				{
					switch (waveNumber)
					{
					case 1: {
						m_level->UpdateWave1();
						break;
					}
					case 2: {
						m_level->UpdateWave2();
						break;
					}
					case 3: {
						if (chicken->IsAtLimit() != Chicken::LimitReached::NONE) {
							changeDirection = chicken->IsAtLimit();
							m_level->UpdateWave3(changeDirection);
						}
						break;
					}
					case 5: {
						if (chicken->IsAtLimit() != Chicken::LimitReached::NONE)
							m_level->UpdateWave5(chicken->IsAtLimit());
						break;
					}
					default:
						break;
					}
					
				}
				
			}
			object->Update();
			it++;
			
		}

	}
	
	if (!existElementsFromWave) 
	{
		if (m_firstWave)
		{
			WaveStringUpdate();
			m_scoreMultiplyer += m_scoreMultiplyerIncreaseRatio;
			m_waveNrClock.restart();
			m_firstWave = false;
			m_showWaveNr = true;
		}
		else
		{
			if (m_waveNrClock.getElapsedTime().asSeconds() > m_waveNrTimeToDisplay + 0.1f)
			{
				WaveStringUpdate();
				m_scoreMultiplyer += m_scoreMultiplyerIncreaseRatio;
				m_waveNrClock.restart();
				m_showWaveNr = true;
			}

		}

		if (m_waveNrClock.getElapsedTime().asSeconds() > m_waveNrTimeToDisplay)
		{
			m_showWaveNr = false;

			if (!m_firstWave)
			{
				if (waveNumber % 10 == 0)
					m_musicPlayer->PlayVictory();
				
				if (waveNumber % 10 == 1)
					m_musicPlayer->StopVictory();
			}

			if (waveNumber % 9 == 0)
				m_musicPlayer->PlayBossFight();
			if (waveNumber % 9 == 1)
				m_musicPlayer->StopBossFight();

			if (waveNumber == 10) {
				m_level = new Level(levelNumber++);
				waveNumber = 0;
			}
			std::vector<Object*> wave = m_level->operator[](waveNumber++);
			for (int index2 = 0; index2 < wave.size(); ++index2)
				m_objects.push_back(wave[index2]);
		}
	}
}

void Game::ScoreUpdate()
{
	std::stringstream m_sstream;
	m_sstream << m_gameScore;
	m_text.setString(m_sstream.str());
}

void Game::WaveStringUpdate()
{
	std::string waveString;
	waveString = MakeStringWaveNr(m_waveNr++);
	m_textWaveNr.setString(waveString);
}

void Game::ColisionManager()
{
	for(auto& firstObject: m_objects)
		for (auto& secondObject : m_objects)
		{

			m_Clock.restart();
			if (firstObject->GetObjectName() == ObjectName::BULLET
				&& secondObject->GetObjectName() == ObjectName::ASTEROID
				&& firstObject->GetSprite().getGlobalBounds().intersects(secondObject->GetSprite().getGlobalBounds()))
			{
				firstObject->DecreaseHealth(1);
				secondObject->DecreaseHealth(firstObject->GetDamage());
				m_musicPlayer->PlayHit();

				if (secondObject->GetHealth() <= 0)
					m_gameScore += 10 * m_scoreMultiplyer;
			}

			if (firstObject->GetObjectName() == ObjectName::BULLET
				&& secondObject->GetObjectName() == ObjectName::BOSS
				&& firstObject->GetSprite().getGlobalBounds().intersects(secondObject->GetSprite().getGlobalBounds()))
			{
				firstObject->DecreaseHealth(1);
				secondObject->DecreaseHealth(firstObject->GetDamage());
				m_musicPlayer->PlayHit();

				if (secondObject->GetHealth() <= 0)
					m_gameScore += 100 * m_scoreMultiplyer;
			}

			if (firstObject->GetObjectName() == ObjectName::BULLET
				&& secondObject->GetObjectName() == ObjectName::CHICKEN
				&& firstObject->GetSprite().getGlobalBounds().intersects(secondObject->GetSprite().getGlobalBounds()))
			{
 				firstObject->DecreaseHealth(1);
				secondObject->DecreaseHealth(firstObject->GetDamage());
				m_musicPlayer->PlayHit();
				if (secondObject->GetHealth() <= 0)
					m_gameScore += 10 * m_scoreMultiplyer;
			}

			if (firstObject->GetObjectName() == ObjectName::BULLET
				&& secondObject->GetObjectName() == ObjectName::BIGEGG
				&& firstObject->GetSprite().getGlobalBounds().intersects(secondObject->GetSprite().getGlobalBounds()))
			{
				firstObject->DecreaseHealth(1);
				secondObject->DecreaseHealth(firstObject->GetDamage());
				m_musicPlayer->PlayHit();
				if (secondObject->GetHealth() <= 0)
					m_gameScore += 10 * m_scoreMultiplyer;
			}

			if (firstObject->GetObjectName() == ObjectName::BULLET
				&& secondObject->GetObjectName() == ObjectName::CHICK
				&& firstObject->GetSprite().getGlobalBounds().intersects(secondObject->GetSprite().getGlobalBounds()))
			{
				firstObject->DecreaseHealth(1);
				secondObject->DecreaseHealth(firstObject->GetDamage());
				m_musicPlayer->PlayHit();
				if (secondObject->GetHealth() <= 0)
					m_gameScore += 20 * m_scoreMultiplyer;
			}

			if (firstObject->GetObjectName() == ObjectName::BULLET
				&& secondObject->GetObjectName() == ObjectName::SUBMARINECHICKEN
				&& firstObject->GetSprite().getGlobalBounds().intersects(secondObject->GetSprite().getGlobalBounds()))
			{
				firstObject->DecreaseHealth(1);
				secondObject->DecreaseHealth(firstObject->GetDamage());
				m_musicPlayer->PlayHit();
				if (secondObject->GetHealth() <= 0)
					m_gameScore += 20 * m_scoreMultiplyer;
			}

			if (firstObject->GetObjectName() == ObjectName::BULLET
				&& secondObject->GetObjectName() == ObjectName::BOUNCINGBIGASTEROID
				&& firstObject->GetSprite().getGlobalBounds().intersects(secondObject->GetSprite().getGlobalBounds()))
			{
				firstObject->DecreaseHealth(1);
				secondObject->DecreaseHealth(firstObject->GetDamage());
				m_musicPlayer->PlayHit();
				if (secondObject->GetHealth() <= 0)
					m_gameScore += 10 * m_scoreMultiplyer;
			}

			if (firstObject->GetObjectName() == ObjectName::BULLET
				&& secondObject->GetObjectName() == ObjectName::BONUS
				&& firstObject->GetSprite().getGlobalBounds().intersects(secondObject->GetSprite().getGlobalBounds()))
			{
				firstObject->DecreaseHealth(1);
				secondObject->DecreaseHealth(firstObject->GetDamage());
				m_musicPlayer->PlayHit();
				if (secondObject->GetHealth() <= 0)
					m_gameScore += 10 * m_scoreMultiplyer;
			}

			if (firstObject->GetObjectName() == ObjectName::EGG
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					Player* player = dynamic_cast<Player*>(secondObject);

					if (!player->GetInvincible())
					{
						firstObject->DecreaseHealth(1);
						secondObject->DecreaseHealth(1);
						player->SetInvincible(true);

						if (!player->GetHealth())
						{
							this->EndGame();
						}

						if (!m_playerWasInvincible)
						{
							player->GetClock().restart();
							m_playerWasInvincible = true;
						}
						else
							if (player->GetClock().getElapsedTime().asSeconds() > 
								player->GetInvincibleDuration() + 0.1)
								player->GetClock().restart();


					}
				}


			if (firstObject->GetObjectName() == ObjectName::ASTEROID
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					Player* player = dynamic_cast<Player*>(secondObject);

					if (!player->GetInvincible())
					{
						firstObject->DecreaseHealth(1);
						secondObject->DecreaseHealth(1);
						player->SetInvincible(true);

						if (!player->GetHealth())
						{
							this->EndGame();
						}

						if (!m_playerWasInvincible)
						{
							player->GetClock().restart();
							m_playerWasInvincible = true;
						}
						else
							if (player->GetClock().getElapsedTime().asSeconds() >
								player->GetInvincibleDuration() + 0.1)
								player->GetClock().restart();

					}
				}
	

			if (firstObject->GetObjectName() == ObjectName::CHICKEN
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					Player* player = dynamic_cast<Player*>(secondObject);

					if (!player->GetInvincible())
					{
						firstObject->DecreaseHealth(1);
						secondObject->DecreaseHealth(1);
						player->SetInvincible(true);

						if (!player->GetHealth())
						{
							this->EndGame();

						}

						if (!m_playerWasInvincible)
						{
							player->GetClock().restart();
							m_playerWasInvincible = true;
						}
						else
							if (player->GetClock().getElapsedTime().asSeconds() >
								player->GetInvincibleDuration() + 0.1)
								player->GetClock().restart();

					}
				}

			if (firstObject->GetObjectName() == ObjectName::BOSS
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					Player* player = dynamic_cast<Player*>(secondObject);

					if (!player->GetInvincible())
					{
						firstObject->DecreaseHealth(1);
						secondObject->DecreaseHealth(1);
						player->SetInvincible(true);

						if (!player->GetHealth())
						{
							this->EndGame();
						}

						if (!m_playerWasInvincible)
						{
							player->GetClock().restart();
							m_playerWasInvincible = true;
						}
						else
							if (player->GetClock().getElapsedTime().asSeconds() >
								player->GetInvincibleDuration() + 0.1)
								player->GetClock().restart();

					}
				}
			
			if (firstObject->GetObjectName() == ObjectName::BIGEGG
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					Player* player = dynamic_cast<Player*>(secondObject);

					if (!player->GetInvincible())
					{
						firstObject->DecreaseHealth(1);
						secondObject->DecreaseHealth(1);
						player->SetInvincible(true);

						if (!player->GetHealth())
						{
							this->EndGame();
						}

						if (!m_playerWasInvincible)
						{
							player->GetClock().restart();
							m_playerWasInvincible = true;
						}
						else
							if (player->GetClock().getElapsedTime().asSeconds() >
								player->GetInvincibleDuration() + 0.1)
								player->GetClock().restart();

					}
				}

			if (firstObject->GetObjectName() == ObjectName::CHICK
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					Player* player = dynamic_cast<Player*>(secondObject);

					if (!player->GetInvincible())
					{
						firstObject->DecreaseHealth(1);
						secondObject->DecreaseHealth(1);
						player->SetInvincible(true);

						if (!player->GetHealth())
						{
							this->EndGame();
						}

						if (!m_playerWasInvincible)
						{
							player->GetClock().restart();
							m_playerWasInvincible = true;
						}
						else
							if (player->GetClock().getElapsedTime().asSeconds() >
								player->GetInvincibleDuration() + 0.1)
								player->GetClock().restart();

					}
				}

			if (firstObject->GetObjectName() == ObjectName::SUBMARINECHICKEN
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					Player* player = dynamic_cast<Player*>(secondObject);

					if (!player->GetInvincible())
					{
						firstObject->DecreaseHealth(1);
						secondObject->DecreaseHealth(1);
						player->SetInvincible(true);

						if (!player->GetHealth())
						{
							this->EndGame();
						}

						if (!m_playerWasInvincible)
						{
							player->GetClock().restart();
							m_playerWasInvincible = true;
						}
						else
							if (player->GetClock().getElapsedTime().asSeconds() >
								player->GetInvincibleDuration() + 0.1)
								player->GetClock().restart();

					}

				}

			if (firstObject->GetObjectName() == ObjectName::BOUNCINGBIGASTEROID
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					Player* player = dynamic_cast<Player*>(secondObject);

					if (!player->GetInvincible())
					{
						firstObject->DecreaseHealth(1);
						secondObject->DecreaseHealth(1);
						player->SetInvincible(true);

						if (!player->GetHealth())
						{
							this->EndGame();
						}

						if (!m_playerWasInvincible)
						{
							player->GetClock().restart();
							m_playerWasInvincible = true;
						}
						else
							if (player->GetClock().getElapsedTime().asSeconds() >
								player->GetInvincibleDuration() + 0.1)
								player->GetClock().restart();

					}

				}

			if (firstObject->GetObjectName() == ObjectName::BONUS
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					Player* player = dynamic_cast<Player*>(secondObject);
					m_gameScore += 25;

					if (!player->GetInvincible())
					{
						firstObject->DecreaseHealth(1);
						secondObject->DecreaseHealth(1);
						player->SetInvincible(true);

						if (!player->GetHealth())
						{
							this->EndGame();
						}

						if (!m_playerWasInvincible)
						{
							player->GetClock().restart();
							m_playerWasInvincible = true;
						}
						else
							if (player->GetClock().getElapsedTime().asSeconds() >
								player->GetInvincibleDuration() + 0.1)
								player->GetClock().restart();

					}

				}

			if (firstObject->GetObjectName() == ObjectName::SHADOWBALL
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					Player* player = dynamic_cast<Player*>(secondObject);

					if (!player->GetInvincible())
					{
						firstObject->DecreaseHealth(1);
						secondObject->DecreaseHealth(1);
						player->SetInvincible(true);

						if (!player->GetHealth())
						{
							this->EndGame();
						}

						if (!m_playerWasInvincible)
						{
							player->GetClock().restart();
							m_playerWasInvincible = true;
						}
						else
							if (player->GetClock().getElapsedTime().asSeconds() >
								player->GetInvincibleDuration() + 0.1)
								player->GetClock().restart();


					}
				}

			if (firstObject->GetObjectName() == ObjectName::CHICKENWING
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					firstObject->DecreaseHealth(1);
					++m_gameScore;
					m_musicPlayer.get()->PlayCatchWing();
				}

			if (firstObject->GetObjectName() == ObjectName::BLUEGIFT
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					firstObject->DecreaseHealth(firstObject->GetHealth());

					Player* player = dynamic_cast<Player*>(secondObject);
					if (player->getBulletType() == BulletType::BLUEBULLET)
					{
						 if (player->getBlueBulletLevel() < player->getMaxBlueBulletLevel())
							player->incrementBulletLevel(BulletType::BLUEBULLET);
					}
					else
					player->SetBulletType(BulletType::BLUEBULLET);

					if (secondObject->GetHealth() <= 0)
						m_gameScore += 30 * m_scoreMultiplyer;
				}
				
			
			if (firstObject->GetObjectName() == ObjectName::REDGIFT
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					firstObject->DecreaseHealth(firstObject->GetHealth());

					Player* player = dynamic_cast<Player*>(secondObject);
					if (player->getBulletType() == BulletType::REDBULLET)
					{
						if (player->getRedBulletLevel() < player->getMaxRedBulletLevel())
							player->incrementBulletLevel(BulletType::REDBULLET);
					}
					else
					player->SetBulletType(BulletType::REDBULLET);

					if (secondObject->GetHealth() <= 0)
						m_gameScore += 30 * m_scoreMultiplyer;
				}

			if (firstObject->GetObjectName() == ObjectName::GREENGIFT
				&& secondObject->GetObjectName() == ObjectName::PLAYER)
				if (Collision(firstObject, secondObject))
				{
					firstObject->DecreaseHealth(firstObject->GetHealth());

					Player* player = dynamic_cast<Player*>(secondObject);
					if (player->getBulletType() == BulletType::GREENBULLET)
					{
						if (player->getGreenBulletLevel() < player->getMaxGreenBulletLevel())
							player->incrementBulletLevel(BulletType::GREENBULLET);
					}
					else
						player->SetBulletType(BulletType::GREENBULLET);

					if (secondObject->GetHealth() <= 0)
						m_gameScore += 30 * m_scoreMultiplyer;
				}
		}
}

void Game::ScreenRender()
{
	window->clear();
	window->draw(m_backgroundSprite);
	window->draw(m_text);

	if(m_showWaveNr)
		window->draw(m_textWaveNr);
	if(m_isAfk)
		window->draw(m_afkHintsText);
	if (m_timeToShootBool)
		window->draw(m_timeToShootText);

	DrawObject();
	window->display();
}

void Game::FirePlaySound(const BulletType& bulletType)
{
	switch (bulletType)
	{
		case BulletType::BLUEBULLET:
		{
			m_musicPlayer->PlayBlueBullet();
		}
		break;
		case BulletType::REDBULLET:
		{
			m_musicPlayer->PlayRedBullet();
		}
		break;
		case BulletType::GREENBULLET:
		{
			m_musicPlayer->PlayGreenBullet();
		}
		break;
	}
}

std::string Game::MakeStringWaveNr(int number)
{
	std::stringstream sstream;
	sstream << "Wave " << number;
	return sstream.str();
}

void Game::EndGame()
{
	EndGameInitialisation();
	

	while (this->window->isOpen())
	{

		m_dt = m_dt_clock.restart().asSeconds();
		sf::Event event;
		while (this->window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				this->window->close();

			if (event.type == sf::Event::KeyReleased)
			{
				if (event.key.code == sf::Keyboard::Enter)
				{
					this->window->close();
					levelNumber = 0;
					waveNumber = 0;
					m_waveNr = 1;
					m_objects.clear();

					m_logger->log(m_playerInput, Logger::Level::PlayerName);
					m_logger->log(std::to_string(m_gameScore), Logger::Level::Score);
					m_playerInput = "";
					m_playerText.setString(m_playerInput);

					MainMenuInitialisation();
				}
				
			}
			if (event.type == sf::Event::TextEntered) {
				m_playerInput += event.text.unicode;
				m_playerText.setString(m_playerInput);
			}
			if (event.type == sf::Keyboard::BackSpace) {
				m_playerInput.erase(m_playerInput.size() - 1, 1);
				m_playerText.setString(m_playerInput);
			}
		}

		EndGameScreenRender();
	}
}

void Game::EndGameInitialisation()
{
	m_musicPlayer->PlayGameOver();
	m_pressToRestartClock.restart();

	SetGameOverText();
	SetHighScoreText();
	SetPlayerInputText();
}

void Game::EndGameScreenRender()
{
	window->clear();

	window->draw(m_backgroundSprite);
	window->draw(m_textGameOver);
	window->draw(m_textHighScore);
	window->draw(m_playerText);

	if (m_pressToRestartClock.getElapsedTime().asSeconds() > m_timeToShowRestartText)
	{
		window->draw(m_pressToRestartText);
		if (m_pressToRestartClock.getElapsedTime().asSeconds() > m_timeToShowRestartText * 2)
			m_pressToRestartClock.restart();
	}

	window->display();
}

void Game::SetGameOverText()
{
	m_textGameOver.setString("Game Over");
	m_textGameOver.setFont(m_font);
	m_textGameOver.setFillColor(sf::Color::White);
	m_textGameOver.setOutlineColor(sf::Color::Blue);
	m_textGameOver.setOutlineThickness(5.f);
	m_textGameOver.setCharacterSize(60);
	m_textGameOver.setStyle(sf::Text::Bold);
	m_textGameOver.setLetterSpacing(2.f);
	m_textGameOver.setPosition(WIDTH / 2 - m_textGameOver.getGlobalBounds().width / 2,
		HEIGHT / 2 - 3 * m_textGameOver.getGlobalBounds().height);
}

void Game::SetHighScoreText()
{
	std::stringstream m_sstream;
	m_sstream << m_gameScore;
	m_textHighScore.setString(m_sstream.str());
	m_textHighScore.setFont(m_font);
	m_textHighScore.setFillColor(sf::Color::White);
	m_textHighScore.setOutlineColor(sf::Color::Blue);
	m_textHighScore.setOutlineThickness(5.f);
	m_textHighScore.setCharacterSize(60);
	m_textHighScore.setStyle(sf::Text::Bold);
	m_textHighScore.setLetterSpacing(2.f);
	m_textHighScore.setPosition(WIDTH / 2 - m_textHighScore.getGlobalBounds().width / 2,
		m_textGameOver.getGlobalBounds().top + 1.5 * m_textGameOver.getGlobalBounds().height);
}

void Game::SetPlayerInputText()
{
	m_playerText.setFont(m_font);
	m_playerText.setFillColor(sf::Color::White);
	m_playerText.setCharacterSize(20);
	m_playerText.setStyle(sf::Text::Bold);
	m_playerText.setLetterSpacing(2.f);
	m_playerText.setPosition(m_textHighScore.getGlobalBounds().width,
		HEIGHT / 2 + m_textHighScore.getGlobalBounds().top);
}

void Game::ScoreInitialisation() {

	std::ifstream file("scores.txt");
	if (!file.is_open()) {
		std::cout << "file does not exist";
	}

	std::string line;
	std::pair<std::string, std::string> name_score;
	while (std::getline(file, line)) {
		auto delimiter = line.find(" ");
		std::string token = line.substr(0, delimiter);
		if (token == "[PlayerName]") {
			name_score.first = line.substr(delimiter + 1);
		}

		else if (token == "[Score]") {
			name_score.second = line.substr(delimiter + 1);
			listOfScores.push_back(name_score);
		}

	}

}

void Game::HintsUpdate()
{
	
	if (m_afkClock.getElapsedTime().asSeconds() > m_afkTimer)
		m_isAfk = true;
	else
		m_isAfk = false;

	for (const auto& object : m_objects)
		if (object->GetObjectName() == ObjectName::ASTEROID
			|| object->GetObjectName() == ObjectName::BIGEGG
			|| object->GetObjectName() == ObjectName::BOSS
			|| object->GetObjectName() == ObjectName::BOUNCINGBIGASTEROID
			|| object->GetObjectName() == ObjectName::CHICK
			|| object->GetObjectName() == ObjectName::CHICKEN
			|| object->GetObjectName() == ObjectName::SUBMARINECHICKEN)
		{
			if (m_player->GetCoordX() >= object->GetCoordX()
				&& m_player->GetCoordX() <= object->GetCoordX() + object->GetSprite().getGlobalBounds().width
				&&m_player->GetCoordY()  > object->GetCoordY() + object->GetSprite().getGlobalBounds().height)
			{
				m_timeToShootBool = true;
				break;
			}
		}
		else
			m_timeToShootBool = false;

}

void Game::HintsInitialisation()
{

	m_afkHintsText.setFont(m_font);
	m_afkHintsText.setString("You can use the arrows to move and spacebar to shoot");
	m_afkHintsText.setFillColor(sf::Color::White);
	m_afkHintsText.setOutlineColor(sf::Color::Blue);
	m_afkHintsText.setOutlineThickness(2.f);
	m_afkHintsText.setCharacterSize(30);
	m_afkHintsText.setPosition(WIDTH / 2 - m_afkHintsText.getGlobalBounds().width / 2,
		HEIGHT / 2 - m_afkHintsText.getGlobalBounds().height / 2 - HEIGHT / 4);

	
	m_timeToShootText.setFont(m_font);
	m_timeToShootText.setString("Shoot");
	m_timeToShootText.setFillColor(sf::Color::White);
	m_timeToShootText.setOutlineColor(sf::Color::Blue);
	m_timeToShootText.setOutlineThickness(2.f);
	m_timeToShootText.setCharacterSize(30);
	m_timeToShootText.setPosition(m_timeToShootText.getGlobalBounds().width *0.5,
		HEIGHT - m_timeToShootText.getGlobalBounds().height *2);

	m_pressToRestartText.setFont(m_font);
	m_pressToRestartText.setString("Type your name and press and press ENTER");
	m_pressToRestartText.setFillColor(sf::Color::White);
	m_pressToRestartText.setOutlineColor(sf::Color::Blue);
	m_pressToRestartText.setOutlineThickness(2.f);
	m_pressToRestartText.setCharacterSize(30);
	m_pressToRestartText.setPosition(WIDTH / 2 - m_pressToRestartText.getGlobalBounds().width / 2,
		HEIGHT - HEIGHT/3.5f);
}


void Game::SetObjectList(std::list<Object*> objectList)
{
	m_objects = objectList;
}

std::list<Object*> Game::GetObjectList() const
{
	return m_objects;
}
