#include "Object.h"

Object::Object()
	:m_angle(0), m_radius(0), m_coordx(0), m_coordy(0), m_dx(0), m_dy(0), m_health(0)
{
}

Object::Object(float coordx, float coordy, float dx, float dy, float radius, float angle, float health)
	: m_angle(angle), m_dx(dx), m_dy(dy), m_coordx(coordx), m_coordy(coordy), m_radius(radius), m_health(health)
{
}

Object::Object(const Object& obj)
{
	*this = obj;
}

Object::Object(Object&& source)
{
	*this = std::move(source);
}


Object::~Object()
{
	//delete this;
	//smart pointers 
}

Object* Object::operator=(const Object& obj)
{
	if (this == &obj) return this;

	this->m_angle = obj.m_angle;
	this->m_dx = obj.m_dx;
	this->m_dy = obj.m_dy;
	this->m_coordx = obj.m_coordx;
	this->m_coordy = obj.m_coordy;
	this->m_radius = obj.m_radius;
	return this;
}

void Object::Draw()
{
}

void Object::Update()
{
}

void Object::Settings(int coordx, int coordy, float angle, int radius)
{
	m_coordx = coordx;
	m_coordy = coordy;
	m_angle = angle;
	m_radius = radius;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void Object::SetRadius(const float& radius)
{
	m_radius = radius;
}

void Object::SetAngle(const float& angle)
{
	m_angle = angle;
}

void Object::DecreaseHealth(uint8_t amount)
{
	m_health -= amount;
}

void Object::SetHealth(const int& health)
{
	m_health = health;
}

float Object::GetCoordX() const
{
	return m_coordx;
}

float Object::GetCoordY() const
{
	return m_coordy;
}

float Object::GetDX() const
{
	return m_dx;
}

float Object::GetDY() const
{
	return m_dy;
}

float Object::GetRadius() const
{
	return m_radius;
}

float Object::GetAngle() const
{
	return m_angle;
}

float Object::GetDamage() const
{
	return m_damage;
}

int Object::GetHealth() const
{
	return m_health;
}

Animation Object::GetAnimation() const
{
	return m_animation;
}

sf::Sprite& Object::GetSprite()
{
	return m_sprite;
}

ObjectName Object::GetObjectName()
{
	return m_name;
}



