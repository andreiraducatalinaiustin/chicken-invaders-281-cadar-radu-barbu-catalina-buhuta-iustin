#pragma once
#include "Object.h"
#include "Gift.h"
#include "ShadowBall.h"
class Bonus :public Object
{
public:
	Bonus(int levelNumber);
	void Update();
	void Draw();
	Gift* MakeGift();
	ShadowBall* MakeShadowBall(float dx, float dy);
	void setTargetLocation(int targetX, int targetY);
	void ResetTimeBeforeNewAttack();
	int GetTimeBeforeNewAttack();
private:
	std::string BonusTexturePath;
	int timeBeforeNewAttack = 100;
	int m_targetX;
	int m_targetY;
};
