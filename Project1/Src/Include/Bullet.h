#pragma once
#include"Object.h"
#include <SFML/Graphics.hpp>

class Bullet : public Object
{
public:
	Bullet();
	void Update();
	void Draw();

	void SetRotatedRightTexture();
	void SetRotatedLeftTexture();

private:

	double m_speed, m_frame;
	std::string SingleBlueBulletPath, singleBlueBulletRotatedLeftPath, singleBlueBulletRotatedRightPath;
	sf::Texture m_textureRotatedRight, m_textureRotatedLeft;
};
