﻿#include "ExplosionTypeA.h"
#include "Animation.h"

ExplosionTypeA::ExplosionTypeA(float coordx, float coordy)
{
	m_name = ObjectName::EXPLOSION;
	m_health = 1;
	m_speed = 0.5;

	m_coordx = coordx;
	m_coordy = coordy;

	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();

	m_gameConfig->get("TexturePathA", TexturePath);
	
	m_texture.loadFromFile(TexturePath);

	    m_texture.loadFromFile(TexturePath);
		m_sprite.setTexture(m_texture);
		
		double startingPixelX(0), startingPixelY(0);
		double picturePixelHeight = 64;
		double firstPicturePixelWidth = 64;

		for (int index1 = 0; index1 < 5; ++index1)
		{
			for (int index = 0; index < 5; ++index)
				m_frames.push_back(sf::IntRect(startingPixelX + index * firstPicturePixelWidth,
					startingPixelY, firstPicturePixelWidth, picturePixelHeight));
			startingPixelY += 64;
		}

		m_sprite.setOrigin(firstPicturePixelWidth / 2, picturePixelHeight / 2);
		
		m_sprite.setPosition(m_coordx, m_coordy);
}


void ExplosionTypeA::Update()
{
	int framesNumber = m_frames.size();
	m_frame += m_speed;
	
	if (m_frame + m_speed > framesNumber)
	{
		m_health = 0;
		return;
	}

	if (m_frame >= framesNumber) m_frame -= framesNumber;
	if (framesNumber > 0) m_sprite.setTextureRect(m_frames[static_cast<int>(m_frame)]);


}

void ExplosionTypeA::Draw()
{

}

