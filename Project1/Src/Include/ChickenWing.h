#pragma once
#include "Object.h"
class ChickenWing :public Object
{
public:
	ChickenWing(float coordx, float coordy);
	void Update();
	void Draw();
private:
	std::string ChickenWingTexturePath;
	int timeBeforeDisapear;
};
