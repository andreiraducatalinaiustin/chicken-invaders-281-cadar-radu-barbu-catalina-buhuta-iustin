#pragma once
#include<iostream>
class IDrawable
{
public:
	virtual void Draw() = 0;
	virtual void Update() = 0;
};
