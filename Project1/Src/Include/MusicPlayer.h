#pragma once
#include <SFML/Audio.hpp>
#include "GameConfig.h"
#include <memory>

class MusicPlayer
{
public:
	MusicPlayer(std::unique_ptr<GameConfig> &m_gameConfig);

	void PlayMainTheme();
	void PauseMainTheme();
	void StopMainTheme();

	void PlayBossFight();
	void StopBossFight();

	void PlayGameOver();
	void StopGameOver();

	void PlayVictory();
	void StopVictory();

	void PlayHit();
	void PlayRedBullet();
	void PlayBlueBullet();
	void PlayGreenBullet();
	void PlayCatchWing();

private:
	sf::Music m_mainTheme, m_mainTheme2, m_bossFight, m_gameOver, m_victory;
	sf::SoundBuffer m_blueBulletBuffer, m_redBulletBuffer, m_greenBulletBuffer, m_hitBuffer,
					m_playerDamageBuffer, m_asteroidDamageBuffer, m_catchWingBuffer;
	sf::Sound m_usedSound;
};
