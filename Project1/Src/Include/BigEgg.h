#pragma once
#include "Object.h"
#include "Chick.h"
class BigEgg :public Object
{
public:
	BigEgg(float coordx, float coordy);
	void Update();
	void Draw();
	Chick* MakeChick(float coordx,float coordy,int levelNumber);
private:
	std::string BigEggTexturePath;
};