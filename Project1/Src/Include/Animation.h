#pragma once

#include <SFML/Graphics.hpp>
#include <vector>

class Animation {
public:
	Animation();
	Animation(float coordX, float coordY, uint8_t numFrames, float speed, float width, float height, sf::Texture texture);

	void SetVariables(float coordX, float coordY, uint8_t numFrames, float speed, float width, float height, sf::Texture texture);

	void SetTexture(sf::Texture texture);
	void update();
	bool isEnd();
	sf::Sprite GetSprite();

private:
	float m_speed;
	float m_frame;
	std::vector<sf::IntRect> m_frames;
	sf::Sprite m_sprite;

};