#include "../Include/Egg.h"

Egg::Egg(float coordx,float coordy)
{
	m_name = ObjectName::EGG;
	m_radius = 5;
	m_health = 1;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("EggTexturePath", EggTexturePath);
	m_gameConfig->get("BrokenEggTexturePath", BrokenEggTexturePath);
	if (!m_texture.loadFromFile(EggTexturePath))
		std::cerr << "Error while loading the texture of Egg \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_coordx = coordx;
	m_coordy = coordy;
	timeBeforeDisapear = 50;
	m_dy = rand() % 3 + 1;
}

void Egg::Update()
{
	if (m_coordy <= HEIGHT - 40)
		m_coordy += m_dy;
	else if (timeBeforeDisapear == 50) {
		m_name = ObjectName::BROKENEGG;
		if (!m_texture.loadFromFile(BrokenEggTexturePath))
			std::cerr << "Error while loading the texture of BrokenEgg \n";
		m_texture.setSmooth(true);
		BrokenEggSprite.setTexture(m_texture);
		m_sprite=BrokenEggSprite;
		m_coordx = m_coordx - 25;
		m_coordy = m_coordy + 17;
		--timeBeforeDisapear;
	}
	else if (timeBeforeDisapear == 0)
		m_health = 0;
	else
		--timeBeforeDisapear;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void Egg::Draw()
{
}
