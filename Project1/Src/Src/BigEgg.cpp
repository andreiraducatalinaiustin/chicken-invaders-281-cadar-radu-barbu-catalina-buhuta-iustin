#include "..\include\BigEgg.h"

BigEgg::BigEgg(float coordx, float coordy)
{
	m_name = ObjectName::BIGEGG;
	m_health = 1;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("BigEggTexturePath", BigEggTexturePath);
	if (!m_texture.loadFromFile(BigEggTexturePath))
		std::cerr << "Error while loading the texture of BigEgg \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_coordx = coordx;
	m_coordy = coordy;
	m_dy = rand() % 3 + 1;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void BigEgg::Update()
{
	if (m_coordy < HEIGHT)
		m_coordy += m_dy;
	else
		m_health = 0;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void BigEgg::Draw()
{
}

Chick* BigEgg::MakeChick(float coordx, float coordy,int levelNumber)
{
	Chick* chick = new Chick(coordx, coordy);
	chick->SetHealth(chick->GetHealth() + levelNumber);
	return chick;
}
