#include "ExplosionTypeC.h"

ExplosionTypeC::ExplosionTypeC(float coordx, float coordy)
{
	m_name = ObjectName::EXPLOSION;
	m_health = 1;
	m_speed = 0.75;

	m_coordx = coordx;
	m_coordy = coordy;

	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();

	m_gameConfig->get("TexturePathC", TexturePath);

	m_texture.loadFromFile(TexturePath);

	m_texture.loadFromFile(TexturePath);
	m_sprite.setTexture(m_texture);

	double startingPixelX(0), startingPixelY(0);
	double picturePixelHeight = 100;
	double firstPicturePixelWidth = 100;

	for (int index1 = 0; index1 < 9; ++index1)
	{
		for (int index = 0; index < 9; ++index)
			m_frames.push_back(sf::IntRect(startingPixelX + index * firstPicturePixelWidth,
				startingPixelY, firstPicturePixelWidth, picturePixelHeight));
		startingPixelY += 100;
	}

	m_sprite.setOrigin(firstPicturePixelWidth / 2, picturePixelHeight / 2);

	m_sprite.setPosition(m_coordx, m_coordy);
}

void ExplosionTypeC::Update()
{
	int framesNumber = m_frames.size();
	m_frame += m_speed;

	if (m_frame + m_speed > framesNumber)
	{
		m_health = 0;
		return;
	}

	if (m_frame >= framesNumber) m_frame -= framesNumber;
	if (framesNumber > 0) m_sprite.setTextureRect(m_frames[static_cast<int>(m_frame)]);
}

void ExplosionTypeC::Draw()
{
}
