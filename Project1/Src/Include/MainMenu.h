#pragma once
#include<iostream>
#include <SFML/Graphics.hpp>
#include "Game.h"
#include "GameConfig.h"



class MainMenu
{
public:
	MainMenu(float width, float heigth);
	void StartMenu(sf::RenderWindow* window);
	void HoverStartButton();
	void HoverQuitButton();
	void ReverseQuitButton();
	void ReverseStartButton();

	sf::Sprite GetStartButtonSprite();
	sf::Sprite GetQuitButtonSprite();
	sf::Sprite GetScoresButtonSprite();



	~MainMenu();

private:
	sf::Sprite m_backgroundImage, m_logoImage, m_startButtonImage
		, m_quitButtonImage, m_scoresButton;
	sf::Texture m_backgroundTexture, m_logoTexture, m_startButtonTexture,
		m_quitButtonTexture, m_quitButtonHoverTexture, m_startButtonHoverTexture, m_scoresTexture;
	std::unique_ptr<GameConfig> m_gameConfig;
	std::string BackgroundTexturePath, LogoTexturePath, StartButtonTexturePath, ScoresTexturePath,
		QuitButtonTexturePath, QuitButtonHoverTexturePath, StartButtonHoverTexturePath
;


};

