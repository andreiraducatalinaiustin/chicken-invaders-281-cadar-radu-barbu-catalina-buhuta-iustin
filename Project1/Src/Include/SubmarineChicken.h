#pragma once
#include "Object.h"
#include "Gift.h"
#include "ChickenDirection.h"
class SubmarineChicken :public Object
{
public:
	SubmarineChicken(ChickenDirection direction);
	void Update();
	void Draw();
	Gift* MakeGift();
	int GetGiftDropChance();
private:
	std::string SubmarineChickenTexturePath;
	int giftDropChance;
	ChickenDirection m_direction;
};