#pragma once
#include "IDrawable.h"
#include"ObjectName.h"
#include "BulletType.h"
#include "Animation.h"
#include "GameConfig.h"
#include "SFML/Audio.hpp"
#include <string>
#include <list>

#define RADIAN 0.017453f
#define WIDTH 1200
#define HEIGHT 650

class Object : public IDrawable
{
public:
	Object();
	Object(float coordx, float coordy, float dx, float dy, float radius, float angle, float health);
	Object(const Object& obj);
	Object(Object&& source);
	~Object();

	Object* operator=(const Object& obj);
	friend std::ostream& operator<<(std::ostream& out, const Object& obj);

	void Draw();
	void Update();

	void Settings(int coordx, int coordy, float angle = 0, int radius = 1);
	virtual void SetRadius(const float& radius);
	virtual void SetAngle(const float& angle);
	virtual void DecreaseHealth(uint8_t amount);
	virtual void SetHealth(const int& health);

	float GetCoordX() const;
	float GetCoordY() const;
	float GetDX() const;
	float GetDY() const;
	float GetRadius() const;
	float GetAngle() const;
	float GetDamage()const;
	int GetHealth() const;
	Animation GetAnimation() const;
	sf::Sprite& GetSprite();
	ObjectName GetObjectName();


protected:
	float m_damage;
	ObjectName m_name;
	float m_coordx;
	float m_coordy;
	float m_dx;
	float m_dy;
	float m_radius;
	float m_angle;
	int m_health;
	Animation m_animation;
	sf::Texture m_texture;
	sf::Sprite m_sprite;
	std::unique_ptr<GameConfig> m_gameConfig;
};

