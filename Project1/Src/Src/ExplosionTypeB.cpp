#include "ExplosionTypeB.h"

ExplosionTypeB::ExplosionTypeB(float coordx, float coordy)
{
	m_name = ObjectName::EXPLOSION;
	m_health = 1;
	m_speed = 0.25;

	m_coordx = coordx;
	m_coordy = coordy;

	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();

	m_gameConfig->get("TexturePathB", TexturePath);

	m_texture.loadFromFile(TexturePath);

	m_texture.loadFromFile(TexturePath);
	m_sprite.setTexture(m_texture);

	double startingPixelX(0), startingPixelY(0);
	double picturePixelHeight = 96;
	double firstPicturePixelWidth = 96;

	for (int index = 0; index < 12; ++index)
		m_frames.push_back(sf::IntRect(startingPixelX + index * firstPicturePixelWidth,
			startingPixelY, firstPicturePixelWidth, picturePixelHeight));


	m_sprite.setOrigin(firstPicturePixelWidth / 2, picturePixelHeight / 2);

	m_sprite.setPosition(m_coordx, m_coordy);
}

void ExplosionTypeB::Update()
{
	int framesNumber = m_frames.size();
	m_frame += m_speed;
	
	if (m_frame + m_speed > framesNumber)
	{
		m_health = 0;
		return;
	}
	
	if (framesNumber > 0) m_sprite.setTextureRect(m_frames[static_cast<int>(m_frame)]);


	if (m_frame >= framesNumber) m_frame -= framesNumber;
}

void ExplosionTypeB::Draw()
{
}
