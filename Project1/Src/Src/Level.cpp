#include "..\Include\Level.h"

Level::Level(const int& levelNumber)
{
	std::vector<Object*> newWave;

	//wave 1 - one chicken

	Chicken* chicken = new Chicken(WIDTH/4, HEIGHT/2, 0, 0, 0, 0, 1, rand() % 10 + 1,
		ChickenState::RANDOM);
	chicken->SetHealth(chicken->GetHealth() + levelNumber);
	m_wave1.push_back(chicken);
	m_waves.push_back(m_wave1);


	//wave 2 and 3 - 4 lines of chickens

	CreateWave2(levelNumber);
	m_waves.push_back(m_wave2);

	CreateWave3(levelNumber);
	m_waves.push_back(m_wave3);

	

	//wave 4 - asteroids
	Asteroid* asteroid;
	for (int index = 0; index < 30+2*levelNumber; ++index)
	{
		asteroid = new Asteroid(levelNumber%3);
		asteroid->SetHealth(asteroid->GetHealth() + levelNumber);
		newWave.push_back(asteroid);
	}
	m_waves.push_back(newWave);
	newWave.clear();


	//wave 5 - ?
	CreateWave5(levelNumber);
	m_waves.push_back(m_wave5);

	//wave 6 - ?
	BigEgg* bigEgg;
	for (int index = 0; index < 15 + levelNumber; ++index)
	{
		bigEgg = new BigEgg(rand() % (WIDTH - 20), 0 - rand() % 200 - 86);
		bigEgg->SetHealth(bigEgg->GetHealth() + levelNumber);
		newWave.push_back(bigEgg);
	}
	m_waves.push_back(newWave);
	newWave.clear();

	//wave 7 - ?
	SubmarineChicken* submarineChicken;
	for (int index = 0; index < 25 + levelNumber * 2; ++index)
	{
		if (index % 2 == 0)
			submarineChicken = new SubmarineChicken(ChickenDirection::LEFT);
		else
			submarineChicken = new SubmarineChicken(ChickenDirection::RIGHT);
		submarineChicken->SetHealth(submarineChicken->GetHealth() + levelNumber);
		newWave.push_back(submarineChicken);
	}
	m_waves.push_back(newWave);
	newWave.clear();

	//wave 8 - bouncing asteroid
	BouncingBigAsteroid* bigAsteroid=new BouncingBigAsteroid();
	bigAsteroid->SetHealth(bigAsteroid->GetHealth() + levelNumber);
	newWave.push_back(bigAsteroid);
	m_waves.push_back(newWave);
	newWave.clear();

	//wave 9 - bonus
	Bonus* bonus = new Bonus(levelNumber);
	newWave.push_back(bonus);
	m_waves.push_back(newWave);
	newWave.clear();

	//wave 10 - boss
	Boss* boss;
	boss = new Boss();
	boss->SetHealth(boss->GetHealth() + levelNumber * 10);
	newWave.push_back(boss);
	m_waves.push_back(newWave);
	newWave.clear();
}

std::vector<Object*> &Level::operator[](const int& position)
{
	return m_waves[position];
}

void Level::DeleteElement(Object* object, const int& wave_number)
{
	std::vector<std::vector<Object*>>::iterator vect;
	std::vector<Object*>::iterator obj;
	int count = 0;
	for (vect = m_waves.begin(); vect != m_waves.end(); vect++) {
		for (obj = vect->begin(); obj != vect->end(); obj++) {
			if (*obj == object) {
				obj = vect->erase(obj);
				delete object;
				object = NULL;
				vect->shrink_to_fit();
				return;
			}
		}
	}

}

void Level::CreateWave2(const int& levelNumber)
{
	srand(time(NULL));
	for (int index = 0; index < 10; index++)
	{
		Chicken* chicken = new Chicken(rand() % (WIDTH - 140 + 1) + 141, rand() % (HEIGHT / 2 - 115) + 116,
			0, 0, 0, rand() % 360, 1 + levelNumber, rand() % 10 + 1, ChickenState::RANDOM);
		m_wave2.push_back(chicken);
	}

}

void Level::CreateWave3(const int& levelNumber)
{
	srand(time(NULL));
	int time;
	for (int index = 0; index < 3; index++)
	{
		for (int index2 = 0; index2 < 5; index2++)
		{
			Chicken* chicken = new Chicken(140 * index2 + 141, 115 * (index +1) + 1, 0, 0, 0, 0, 1 + levelNumber, rand() % 10 + 1,
				ChickenState::GROUPED);
			time = rand() % 250 + 200;
			chicken->SetTimeBeforeNewEgg(time + index2 * 100);
			m_wave3.push_back(chicken);
		}
	}
}

void Level::CreateWave5(const int& levelNumber)
{
	srand(time(NULL));
	int time;
	for (int index = 0; index < 3; index++)
	{
		for (int index2 = 0; index2 < 5; index2++)
		{
			Chicken* chicken = new Chicken(140 * index2 + 141, 115 * (index) + 20, 0, 0, 0, 0, 1 + levelNumber, rand() % 10 + 1,
				ChickenState::GROUPED);
			time = rand() % 250 + 200;
			chicken->SetTimeBeforeNewEgg(time + index2 * 100);

			m_wave5.push_back(chicken);
		}
	}
}



void Level::UpdateWave1()
{
	int new_angle;
	std::vector<int>angles{
		//down
		0, 45, 90, 135, 180,
		//up
		180, 225, 270, 315, 0,
		//left
		270, 315, 0, 45, 90,
		//right
		90, 135, 180, 225
		//upper-right

	};
	auto it = m_waves[0].begin();
	Chicken* chicken = dynamic_cast<Chicken*>(*it);
	if (chicken->GetTimeBeforeNewDirection() == 0 || 
		chicken->IsAtLimit() != Chicken::LimitReached::NONE) {
		chicken->SetTimeBeforeNewDirection(rand() % 250 + 100);
		switch (chicken->IsAtLimit())
		{
		case Chicken::LimitReached::LEFT:
		{
			new_angle = angles[rand() % (14 - 10 + 1) + 10];
			break;
		}
		case Chicken::LimitReached::RIGHT:
		{
			new_angle = angles[rand() % (18 - 15 + 1) + 15];
			break;
		}
		case Chicken::LimitReached::DOWN:
		{
			new_angle = angles[rand() % (9 - 5 + 1) + 5];
			break;
		}
		case Chicken::LimitReached::UP:
		{
			new_angle = angles[rand() % 5];
			break;
		}

		default:
			new_angle = rand() % 360;
			break;
		}
		chicken->SetAngle(new_angle);
	}
}

void Level::UpdateWave3(const Chicken::LimitReached& limit)
{
	auto it = m_waves[2].begin();
	
	while (it != m_waves[2].end()) {
		Object* object = *it;
		if (object->GetObjectName() == ObjectName::CHICKEN) {

			Chicken* chicken = dynamic_cast<Chicken*>(*it);
			std::pair<int, int>newDirection;

			switch (limit)
			{
			case Chicken::LimitReached::LEFT:
			{
				chicken->SetAngle(0);
				break;
			}
			case Chicken::LimitReached::RIGHT:
			{
				chicken->SetAngle(180);
				break;
			}

			default:
				break;
			}

		}

		it++;
	}
}

void Level::UpdateWave2()
{
	int new_angle;
	std::vector<int>angles{
		//down
		0, 45, 90, 135, 180,
		//up
		180, 225, 270, 315, 0,
		//left
		270, 315, 0, 45, 90,
		//right
		90, 135, 180, 225
		//upper-right

	};
	auto it = m_waves[1].begin();
	while (it != m_waves[1].end()) {
		Chicken* chicken = dynamic_cast<Chicken*>(*it);
		if (chicken->GetTimeBeforeNewDirection() == 0 ||
			chicken->IsAtLimit() != Chicken::LimitReached::NONE) {
			chicken->SetTimeBeforeNewDirection(rand() % 250 + 100);
			switch (chicken->IsAtLimit())
			{
			case Chicken::LimitReached::LEFT:
			{
				new_angle = angles[rand() % (14 - 10 + 1) + 10];
				break;
			}
			case Chicken::LimitReached::RIGHT:
			{
				new_angle = angles[rand() % (18 - 15 + 1) + 15];
				break;
			}
			case Chicken::LimitReached::DOWN:
			{
				new_angle = angles[rand() % (9 - 5 + 1) + 5];
				break;
			}
			case Chicken::LimitReached::UP:
			{
				new_angle = angles[rand() % 5];
				break;
			}

			default:
				new_angle = rand() % 360;
				break;
			}
			chicken->SetAngle(new_angle);
		}

		it++;
	}
}

void Level::UpdateWave5(const Chicken::LimitReached& limit)
{
	srand(time(NULL));
;	auto it = m_waves[4].begin();
	int new_angle;
	std::vector<int>angles{
		//down
		0, 45, 90, 135, 180, 
		//up
		180, 225, 270, 315, 0,
		//left
		270, 315, 0, 45, 90,
		//right
		90, 135, 180, 225
		//upper-right

	};
	Chicken* chicken = dynamic_cast<Chicken*>(*it);

	switch (limit)
	{
	case Chicken::LimitReached::LEFT:
	{
		new_angle = angles[rand() % (14 - 10 + 1) +10];
		break;
	}
	case Chicken::LimitReached::RIGHT:
	{
		new_angle = angles[rand() % (18 - 15 + 1) + 15];
		break;
	}
	case Chicken::LimitReached::DOWN:
	{
		new_angle = angles[rand() % (9 - 5 + 1) + 5];
		break;
	}
	case Chicken::LimitReached::UP:
	{
		new_angle = angles[rand() % 5];
		break;
	}

	default:
		break;
	}

	while (it != m_waves[4].end()) {
		Object* object = *it;
		if (object->GetObjectName() == ObjectName::CHICKEN) {

			Chicken* chicken = dynamic_cast<Chicken*>(*it);
			chicken->SetAngle(new_angle);
			

		}

		it++;
	}
}


