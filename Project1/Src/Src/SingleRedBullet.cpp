#include "..\Include\SingleRedBullet.h"

SingleRedBullet::SingleRedBullet()
{
	m_name = ObjectName::BULLET;
	m_health = 1;
	m_damage = 2;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("SingleRedBulletPath", SingleRedBulletPath);
	m_gameConfig->get("SingleRedBulletRotatedLeftPath", SingleRedBulletRotatedLeftPath);
	m_gameConfig->get("SingleRedBulletRotatedRightPath", SingleRedBulletRotatedRightPath);

	m_texture.loadFromFile(SingleRedBulletPath);
	m_textureRotatedLeft.loadFromFile(SingleRedBulletRotatedLeftPath);
	m_textureRotatedRight.loadFromFile(SingleRedBulletRotatedRightPath);

	m_texture.setSmooth(true);
	m_textureRotatedLeft.setSmooth(true);
	m_textureRotatedRight.setSmooth(true);
	m_sprite.setTexture(m_texture);
}

void SingleRedBullet::Update()
{

	m_dx = cos(m_angle * RADIAN) * 6;
	m_dy = sin(m_angle * RADIAN) * 6;
	m_coordx += m_dx;
	m_coordy += m_dy;

	if (m_coordx > WIDTH || m_coordx<0 || m_coordy> HEIGHT || m_coordy < 0)
		m_health = 0;

	m_sprite.setPosition(m_coordx, m_coordy);
}

void SingleRedBullet::Draw()
{
}

void SingleRedBullet::SetRotatedRightTexture()
{
	m_sprite.setTexture(m_textureRotatedRight);
}

void SingleRedBullet::SetRotatedLeftTexture()
{
	m_sprite.setTexture(m_textureRotatedLeft);
}
