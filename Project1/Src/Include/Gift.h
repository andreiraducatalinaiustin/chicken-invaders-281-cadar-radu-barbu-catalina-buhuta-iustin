#pragma once
#include "Object.h"
class Gift :public Object 
{
public:
	Gift(float coordx, float coordy);
	void Update();
	void Draw();
private:
	std::string GiftTexturePath;
	int giftType;
};