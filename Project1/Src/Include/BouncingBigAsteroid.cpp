#include "BouncingBigAsteroid.h"

BouncingBigAsteroid::BouncingBigAsteroid()
{
	m_name = ObjectName::BOUNCINGBIGASTEROID;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("BigAsteroidTexturePath", BigAsteroidTexturePath);
	if (!m_texture.loadFromFile(BigAsteroidTexturePath))
		std::cerr << "Error while loading the texture of BigAsteroid \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_sprite.setTextureRect(sf::IntRect(0, 0, 91, 91));
	m_textureX = 0;
	m_textureY = 0;
	m_health = 5;
	m_coordx = WIDTH / 2;
	m_dx = rand() % 4 + 2;
	m_dy = rand() % 4 + 2;
	m_sprite.setPosition(m_coordx, m_coordy);
}

void BouncingBigAsteroid::Update()
{
	m_coordx += m_dx;
	m_coordy += m_dy;
	if (m_coordx + 91 > WIDTH || m_coordx < 0)
		m_dx = -m_dx;
	if (m_coordy + 91 > HEIGHT || m_coordy < 0)
		m_dy = -m_dy;
	if (m_textureX == 91 * 7)
		if (m_textureY == 91 * 7)
		{
			m_textureX = 0;
			m_textureY = 0;
		}
		else {
			m_textureX = 0;
			m_textureY += 91;
		}
	else
		m_textureX += 91;
	m_sprite.setTextureRect(sf::IntRect(m_textureX, m_textureY, 91, 91));

	m_sprite.setPosition(m_coordx, m_coordy);
}

void BouncingBigAsteroid::Draw()
{
}

BouncingAsteroid* BouncingBigAsteroid::MakeSmallAsteroid(float coordx, float coordy,int levelNumber)
{
	BouncingAsteroid* bouncingAsteroid = new BouncingAsteroid(coordx, coordy);
	bouncingAsteroid->SetHealth(5 + levelNumber);
	return bouncingAsteroid;
}
