#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>
#include "GameConfig.h"

class ScoreWindow
{
public:
	ScoreWindow(const float& width,const float& height);
	void StartScoreWindow(sf::RenderWindow* window);

	void SetScores(std::vector<std::pair<std::string, std::string>> listOfScores,
		const float& width, const float& height);
	sf::Text GetText();
	

private:
	sf::Sprite m_backToMenuButton, m_backgroundImage;
	sf::Texture m_backToMenuTexture, m_backgroundTexture;
	std::unique_ptr<GameConfig> m_gameConfig;
	std::string m_backToMenuString, m_backgroundTexturePath;
	sf::Font m_font;
	sf::Text m_info;

	std::vector<sf::Text> m_scores;

};

