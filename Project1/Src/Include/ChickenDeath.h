#pragma once
#include "Object.h"
class ChickenDeath : public Object
{
public:
	ChickenDeath(float coordx, float coordy);
	void Update();
	void Draw();
private:
	double m_speed, m_frame;
	std::string TexturePathChickenDeath;
	std::vector<sf::IntRect> m_frames;
};
