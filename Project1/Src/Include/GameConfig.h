#pragma once
#include <string>
#include <map>
#include <sstream>

class GameConfig {
public:
	GameConfig();
	GameConfig(const std::string& filePath);
	~GameConfig();
	template<typename T>
	void get(const std::string& key, T& value) const;
private:
	template<typename T>
	T convertToType(const std::string& input) const;
	std::map<std::string, std::string> m_data;
};

template <typename T>
inline T GameConfig::convertToType(const std::string& input) const {
	throw "Unconvertable type encountered, please use a different type, or define the handle case in GameConfig.h";
}

template<>
inline int GameConfig::convertToType<int>(const std::string& input) const {
	int value;
	std::stringstream ss(input);
	ss >> value;

	return value;
}

template<>
inline double GameConfig::convertToType<double>(const std::string& input) const {
	double value;
	std::stringstream ss(input);
	ss >> value;

	return value;
}

template<>
inline float GameConfig::convertToType<float>(const std::string& input) const {
	float value;
	std::stringstream ss(input);
	ss >> value;

	return value;
}

template<>
inline short GameConfig::convertToType<short>(const std::string& input) const {
	short value;
	std::stringstream ss(input);
	ss >> value;

	return value;
}

template<>
inline bool GameConfig::convertToType<bool>(const std::string& input) const {
	return input == "TRUE" ? true : false;
}

template<>
inline char GameConfig::convertToType<char>(const std::string& input) const {
	return input[0];
}

template<>
inline std::string GameConfig::convertToType<std::string>(const std::string& input) const {
	return input;
}

template<typename T>
void GameConfig::get(const std::string& key, T& value) const {
	auto it = m_data.find(key);

	if (it != m_data.end()) {
		value = convertToType<T>(m_data.at(key));
	}
}