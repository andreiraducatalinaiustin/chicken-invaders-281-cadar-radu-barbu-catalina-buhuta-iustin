#pragma once
#include "Object.h"
#include "BouncingAsteroid.h"
class BouncingBigAsteroid :public Object
{
public:
	BouncingBigAsteroid();
	void Update();
	void Draw();
	BouncingAsteroid* MakeSmallAsteroid(float coordx, float coordy, int levelNumber);
private:
	int m_textureX;
	int m_textureY;
	std::string BigAsteroidTexturePath;
};