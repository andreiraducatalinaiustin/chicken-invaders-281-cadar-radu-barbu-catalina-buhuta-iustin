#pragma once
#include "Object.h"
class Egg : public Object
{
public:
	Egg(float coordx, float coordy);
	void Update();
	void Draw();
private:
	std::string EggTexturePath, BrokenEggTexturePath;
	int timeBeforeDisapear;
	sf::Sprite BrokenEggSprite;
};
