#include "..\Include\Boss.h"

Boss::Boss()
{
	m_name = ObjectName::BOSS;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("BossTexturePath", BossTexturePath);
	if (!m_texture.loadFromFile(BossTexturePath))
		std::cerr << "Error while loading the texture of Boss \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_coordx = WIDTH / 2;
	m_health = 30;
	
	timeBeforeChangeDiraction = rand() % 50+50;
	timeBeforeNewEgg = rand() % 30 + 5;
	nrChickenWings = rand() % 10 + 5;

	m_textureX = 0;
	m_textureY = 0;
	m_sprite.setTextureRect(sf::IntRect(0, 0, 203.5, 181));
	m_sprite.setPosition(m_coordx, m_coordy);
}


Boss::Boss(float coordx, float coordy, float dx, float dy, float radius, float angle, float health):
	Object(coordx,coordy,dx,dy,radius,angle,health)
{
	m_name = ObjectName::BOSS;
}

void Boss::Update()
{
	if (m_textureX == 203.5 * 3) {
		if (m_textureY == 181 * 2) {
			m_textureX = 0;
			m_textureY = 0;
		}
		else {
			m_textureX = 0;
			m_textureY += 181;
		}
	}
	else {
		m_textureX += 203.5;
	}

	m_sprite.setTextureRect(sf::IntRect(m_textureX, m_textureY, 203.5, 181));

	

	if (m_coordx <= 0)
	{
		m_dx = rand() % 2;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}
	else if (m_coordx >= WIDTH - m_sprite.getGlobalBounds().width)
	{
		m_dx = rand() % 2 - 1;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}
	else if(timeBeforeChangeDiraction==0)
	{
		m_dx = rand() % 10 - 4;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}

	if (m_coordy <= 0)
	{
		m_dy = rand() % 2;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}
	else if (m_coordy>= HEIGHT - m_sprite.getGlobalBounds().height)
	{
		m_dy = rand() % 2 - 1;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}
	else if(timeBeforeChangeDiraction == 0)
	{
		m_dy = rand() % 3 - 1;
		timeBeforeChangeDiraction = rand() % 50 + 50;
	}
	if (timeBeforeNewEgg == 0)
		ResetTimeBeforeNewEgg();
	m_coordx += m_dx;
	m_coordy += m_dy;
	--timeBeforeChangeDiraction;
	--timeBeforeNewEgg;
	m_sprite.setPosition(m_coordx, m_coordy);
}

Egg* Boss::MakeANewEgg() {
	Egg* egg = new Egg(m_coordx + 100, m_coordy + 120);
	return egg;
}

int Boss::GetNrChickenWings()
{
	return nrChickenWings;
}

ChickenWing* Boss::MakeChickenWing()
{
	ChickenWing* chickenWing = new ChickenWing(m_coordx+100, m_coordy+60);
	return chickenWing;
}

Gift* Boss::MakeGift()
{
	Gift* gift = new Gift(m_coordx + 100, m_coordy + 60);
	return gift;
}

int Boss::GetGiftDropChance()
{
	giftDropChance = rand() % 2;
	return giftDropChance;
}

void Boss::Draw()
{
}

int Boss::GetTimeBeforeNewEgg()
{
	return timeBeforeNewEgg;
}

void Boss::ResetTimeBeforeNewEgg()
{
	timeBeforeNewEgg = rand() % 30 + 10;
}
