#include "pch.h"
#include "CppUnitTest.h"
#include "../Src/Include/GameConfig.h"
#include "../Src/Include/Object.h"
#include "../Src/Include/Animation.h"
#include <iostream>
#include "Bullet.h"
#include "SingleRedBullet.h"
#include "SingleRedBullet.h"
#include "Asteroid.h"
#include "Chicken.h"
#include "Game.h"
#include "Player.h"
#include "Boss.h"
#include <memory>
#include "SFML/Graphics.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(GameConfigGetStringTest)
		{
			auto gameConfig = std::make_unique<GameConfig>("..\\UnitTest1\\GameConfig.cfg");

			std::string stringValue = "";
			gameConfig->get("StringValue", stringValue);
			Assert::AreEqual("StringValue", stringValue.c_str());
		}

		TEST_METHOD(GameConfigGetIntTest)
		{
			auto gameConfig = std::make_unique<GameConfig>("..\\UnitTest1\\GameConfig.cfg");


			int intValue = 0;
			gameConfig->get("IntValue", intValue);
			Assert::AreEqual(1, intValue);
		}

		TEST_METHOD(GameConfigGetFloatTest)
		{
			auto gameConfig = std::make_unique<GameConfig>("..\\UnitTest1\\GameConfig.cfg");


			int floatValue = 0;
			gameConfig->get("FloatValue", floatValue);
			Assert::AreEqual(1, floatValue);
		}

		
		TEST_METHOD(GameConfigGetCharTest)
		{
			auto gameConfig = std::make_unique<GameConfig>("..\\UnitTest1\\GameConfig.cfg");
			char charValue = '0';

			gameConfig->get("CharValue", charValue);
			Assert::AreEqual('a', charValue);
		}
		
		TEST_METHOD(ObjectTest)
		{
			auto obj = std::make_unique<Object>(100, 100, 10, 10, 5, 270, 1);

			Assert::AreEqual(1, obj->GetHealth());
		}

		TEST_METHOD(CollisionBlueBulletAsteroid)
		{
			auto bullet = std::make_unique<Bullet>();
			bullet->Settings(300, 200, 270, 1);

			auto asteroid = std::make_unique<Asteroid>(300, 200, 10, 270, 1);

			auto game = std::make_unique<Game>();
			bool isCollision = false;
			isCollision = game->Collision(bullet.get(), asteroid.get());
			Assert::IsTrue(isCollision);

		}

		TEST_METHOD(NotCollisionBlueBulletAsteroid)
		{
			auto bullet = std::make_unique<Bullet>();
			bullet->Settings(10, 10, 270, 1);

			auto asteroid = std::make_unique<Asteroid>(300, 200, 10, 270, 1);

			auto game = std::make_unique<Game>();
			bool isCollision = true;
			isCollision = game->Collision(bullet.get(), asteroid.get());
			Assert::IsFalse(isCollision);

		}


		TEST_METHOD(CollisionRedBulletAsteroid)
		{
			auto bullet = std::make_unique<SingleRedBullet>();
			bullet->Settings(300, 200, 270, 1);

			auto asteroid = std::make_unique<Asteroid>(300, 200, 10, 270, 1);

			auto game = std::make_unique<Game>();
			bool isCollision = false;
			isCollision = game->Collision(bullet.get(), asteroid.get());
			Assert::IsTrue(isCollision);

		}
		
		TEST_METHOD(NotCollisionRedBulletAsteroid)
		{
			auto bullet = std::make_unique<SingleRedBullet>();
			bullet->Settings(10, 10, 270, 1);

			auto asteroid = std::make_unique<Asteroid>(300, 200, 10, 270, 1);

			auto game = std::make_unique<Game>();
			bool isCollision = true;
			isCollision = game->Collision(bullet.get(), asteroid.get());
			Assert::IsFalse(isCollision);

		}

		TEST_METHOD(CollisionGreenBulletChicken)
		{
			auto bullet = std::make_unique<SingleGreenBullet>();
			bullet->Settings(300, 200, 270, 1);

			auto chicken = std::make_unique<Chicken>(300, 200, 1, 1, 10, 270, 1, 3, ChickenState::STAY);

			auto game = std::make_unique<Game>();
			bool isCollision = false;
			isCollision = game->Collision(bullet.get(), chicken.get());
			Assert::IsTrue(isCollision);

		}

		TEST_METHOD(CollisionRedBulletChicken)
		{
			auto bullet = std::make_unique<SingleRedBullet>();
			bullet->Settings(300, 200, 270, 1);

			auto chicken = std::make_unique<Chicken>(300, 200, 1, 1, 10, 270, 1, 3, ChickenState::STAY);

			auto game = std::make_unique<Game>();
			bool isCollision = false;
			isCollision = game->Collision(bullet.get(), chicken.get());
			Assert::IsTrue(isCollision);

		}

		TEST_METHOD(NotCollisionRedBulletChicken)
		{
			auto bullet = std::make_unique<SingleRedBullet>();
			bullet->Settings(30, 20, 270, 1);

			auto chicken = std::make_unique<Chicken>(300, 200, 1, 1, 10, 270, 1, 3, ChickenState::STAY);

			auto game = std::make_unique<Game>();
			bool isCollision = true;
			isCollision = game->Collision(bullet.get(), chicken.get());
			Assert::IsFalse(isCollision);

		}

		TEST_METHOD(NotCollisionGreenBulletChicken)
		{
			auto bullet = std::make_unique<SingleGreenBullet>();
			bullet->Settings(10, 20, 270, 1);

			auto chicken = std::make_unique<Chicken>(300, 200, 1, 1, 10, 270, 1, 3, ChickenState::STAY);

			auto game = std::make_unique<Game>();
			bool isCollision = true;
			isCollision = game->Collision(bullet.get(), chicken.get());
			Assert::IsFalse(isCollision);

		}

		TEST_METHOD(CollisionGreenBulletBoss)
		{
			auto bullet = std::make_unique<SingleGreenBullet>();
			bullet->Settings(300, 200, 270, 1);

			auto boss = std::make_unique<Boss>(300, 200, 1, 1, 10, 270, 1);

			auto game = std::make_unique<Game>();
			bool isCollision = false;
			isCollision = game->Collision(bullet.get(), boss.get());
			Assert::IsTrue(isCollision);

		}

		TEST_METHOD(NotCollisionGreenBulletBoss)
		{
			auto bullet = std::make_unique<SingleGreenBullet>();
			bullet->Settings(10, 20, 270, 1);

			auto boss = std::make_unique<Boss>(300, 200, 1, 1, 10, 270, 1);

			auto game = std::make_unique<Game>();
			bool isCollision = true;
			isCollision = game->Collision(bullet.get(), boss.get());
			Assert::IsFalse(isCollision);

		}

		TEST_METHOD(CollisionPlayerChicken)
		{
			auto player = std::make_unique<Player>();
			player->Settings(300, 200, 270, 1);

			auto chicken = std::make_unique<Chicken>(300, 200, 1, 1, 10, 270, 1, 3, ChickenState::STAY);

			auto game = std::make_unique<Game>();
			bool isCollision = false;
			isCollision = game->Collision(player.get(), chicken.get());
			Assert::IsTrue(isCollision);

		}

		TEST_METHOD(NotCollisionPlayerChicken)
		{
			auto player = std::make_unique<Player>();
			player->Settings(30, 20, 270, 1);

			auto chicken = std::make_unique<Chicken>(300, 200, 1, 1, 10, 270, 1, 3, ChickenState::STAY);

			auto game = std::make_unique<Game>();
			bool isCollision = true;
			isCollision = game->Collision(player.get(), chicken.get());
			Assert::IsFalse(isCollision);

		}

		TEST_METHOD(CollisionPlayerAsteroid)
		{
			auto player = std::make_unique<Player>();
			player->Settings(300, 200, 270, 1);

			auto asteroid = std::make_unique<Asteroid>(300, 200, 10, 270, 1);

			auto game = std::make_unique<Game>();
			bool isCollision = false;
			isCollision = game->Collision(player.get(), asteroid.get());
			Assert::IsTrue(isCollision);

		}

		TEST_METHOD(NotCollisionPlayerAsteroid)
		{
			auto player = std::make_unique<Player>();
			player->Settings(30, 20, 270, 1);

			auto asteroid = std::make_unique<Asteroid>(300, 200, 10, 270, 1);

			auto game = std::make_unique<Game>();
			bool isCollision = true;
			isCollision = game->Collision(player.get(), asteroid.get());
			Assert::IsFalse(isCollision);

		}

		TEST_METHOD(CollisionPlayerBoss)
		{
			auto player = std::make_unique<Player>();
			player->Settings(300, 200, 270, 1);

			auto boss = std::make_unique<Boss>(300, 200, 1, 1, 10, 270, 1);

			auto game = std::make_unique<Game>();
			bool isCollision = false;
			isCollision = game->Collision(player.get(), boss.get());
			Assert::IsTrue(isCollision);

		}

		TEST_METHOD(NotCollisionPlayerBoss)
		{
			auto player = std::make_unique<Player>();
			player->Settings(30, 20, 270, 1);

			auto boss = std::make_unique<Boss>(300, 200, 1, 1, 10, 270, 1);

			auto game = std::make_unique<Game>();
			bool isCollision = true;
			isCollision = game->Collision(player.get(), boss.get());
			Assert::IsFalse(isCollision);

		}

		TEST_METHOD(CollisionPlayerEgg)
		{
			auto player = std::make_unique<Player>();
			player->Settings(300, 200, 270, 1);

			auto egg = std::make_unique<Egg>(300, 200);

			auto game = std::make_unique<Game>();
			bool isCollision = false;
			isCollision = game->Collision(player.get(), egg.get());
			Assert::IsTrue(isCollision);

		}

		TEST_METHOD(NotCollisionPlayerEgg)
		{
			auto player = std::make_unique<Player>();
			player->Settings(30, 20, 270, 1);

			auto egg = std::make_unique<Egg>(300, 200);

			auto game = std::make_unique<Game>();
			bool isCollision = true;
			isCollision = game->Collision(player.get(), egg.get());
			Assert::IsFalse(isCollision);

		}

		TEST_METHOD(CollisionPlayerChickenWing)
		{
			auto game = std::make_unique<Game>();

			auto player = std::make_unique<Player>();
			player->Settings(300, 200, 270, 1);

			auto chickenWing = std::make_unique<ChickenWing>(300, 200);
			
			bool isCollision = false;
			isCollision = game->Collision(player.get(), chickenWing.get());

			Assert::IsTrue(isCollision);

		}

		TEST_METHOD(NotCollisionPlayerChickenWing)
		{
			auto game = std::make_unique<Game>();

			auto player = std::make_unique<Player>();
			player->Settings(300, 200, 270, 1);

			auto chickenWing = std::make_unique<ChickenWing>(400, 200);

			bool isCollision = true;
			isCollision = game->Collision(player.get(), chickenWing.get());

			Assert::IsFalse(isCollision);

		}

		TEST_METHOD(CollisionPlayerGift)
		{
			auto game = std::make_unique<Game>();

			auto player = std::make_unique<Player>();
			player->Settings(300, 200, 270, 1);

			auto gift = std::make_unique<Gift>(300, 200);

			bool isCollision = true;
			isCollision = game->Collision(player.get(), gift.get());

			Assert::IsTrue(isCollision);

		}

		TEST_METHOD(NotCollisionPlayerGift)
		{
			auto game = std::make_unique<Game>();

			auto player = std::make_unique<Player>();
			player->Settings(300, 200, 270, 1);

			auto gift = std::make_unique<Gift>(400, 200);

			bool isCollision = true;
			isCollision = game->Collision(player.get(), gift.get());

			Assert::IsFalse(isCollision);

		}

		TEST_METHOD(NotCollisionPlayerSubmarineChicken)
		{
			auto game = std::make_unique<Game>();

			auto player = std::make_unique<Player>();
			player->Settings(300, 200, 270, 1);

			auto submarineChicken = std::make_unique<SubmarineChicken>(ChickenDirection::LEFT);

			bool isCollision = true;
			isCollision = game->Collision(player.get(), submarineChicken.get());

			Assert::IsFalse(isCollision);

		}
	};
}
