#include "Game.h"
#include <fstream>
#include "../Logging.h"

int main()
{
	std::unique_ptr<Game> game = std::make_unique<Game>();
	std::ofstream of("syslog.dll", std::ios::app);
	Logger logger(of);
	logger.log("Started game", Logger::Level::Info);
	game->MainMenuInitialisation();

	return 0;
}