#include "ScoreWindow.h"

ScoreWindow::ScoreWindow(const float& width, const float& height)
{
	m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("BackgroundTexturePath", m_backgroundTexturePath);
	if (!m_backgroundTexture.loadFromFile(m_backgroundTexturePath))
		std::cerr << "Error while loading the texture of Background \n";


	m_backgroundTexture.setSmooth(true);
	m_backgroundImage.setTexture(m_backgroundTexture);

	sf::Vector2f backgrTargetSize(width, height);
	m_backgroundImage.setScale(backgrTargetSize.x / m_backgroundImage.getLocalBounds().width,
		backgrTargetSize.y / m_backgroundImage.getLocalBounds().height);

	m_info.setString("HALL OF FAME");
	m_info.setFont(m_font);
	m_info.setFillColor(sf::Color::White);
	m_info.setOutlineColor(sf::Color::Blue);
	m_info.setOutlineThickness(5.f);
	m_info.setCharacterSize(50);
	m_info.setStyle(sf::Text::Bold);
	m_info.setLetterSpacing(2.f);
	m_info.setPosition(width / 4 - m_info.getGlobalBounds().width /2 ,
		5);


}

void ScoreWindow::StartScoreWindow(sf::RenderWindow* window)
{
	window->setFramerateLimit(60);
	window->clear();
	
	window->draw(this->m_backgroundImage);
	window->draw(this->m_info);
	for (int index = 0; index < m_scores.size(); index++)
	{
		window->draw(m_scores[index]);
	}
	window->display();

}

bool compare(std::pair<std::string, std::string> first,
	std::pair<std::string, std::string> second) {
	return (atoi(first.second.c_str()) > atoi(second.second.c_str()));
}

void ScoreWindow::SetScores(std::vector<std::pair<std::string, std::string>> listOfScores,
	const float& width, const float& height)
{
	std::string font;
	m_gameConfig->get("Font", font);
	m_font.loadFromFile(font);
	auto count = 0;

	std::sort(listOfScores.begin(), listOfScores.end(), compare);
	if (listOfScores.size() > 10) {
		listOfScores.resize(listOfScores.size() - (listOfScores.size() - 10));
	}

	auto it = listOfScores.begin();
	while (it != listOfScores.end()) {
		sf::Text score;
		score.setString(it->first + "   ----   " + it->second);
		score.setFont(m_font);
		score.setFillColor(sf::Color::White);
		score.setOutlineColor(sf::Color::Blue);
		score.setOutlineThickness(5.f);
		score.setCharacterSize(20);
		score.setStyle(sf::Text::Bold);
		score.setLetterSpacing(2.f);
		score.setPosition(width / 2 - score.getGlobalBounds().width / 2,
			height / 6 + score.getLocalBounds().height*count*2);
		count++;
		m_scores.push_back(score);
		it++;
	}

}

sf::Text ScoreWindow::GetText()
{
	return m_info;
}

