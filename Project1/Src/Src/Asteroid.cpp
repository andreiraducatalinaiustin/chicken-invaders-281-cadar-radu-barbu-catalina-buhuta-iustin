#include "..\Include\Asteroid.h"

Asteroid::Asteroid(int mod)
{
	m_mod = mod;
	if (m_mod == 0)
	{
		m_coordx = rand() % WIDTH - WIDTH / 3;
		m_coordy = -rand() % 250-50;
		m_dx = 1.5;
		m_dy = rand() % 4 + 2;
	}
	else if (mod == 1)
	{
		m_coordx = rand() % WIDTH + WIDTH / 3;
		m_coordy = -rand() % 250-50;
		m_dx = -1.5;
		m_dy = rand() % 4 + 2;
	}
	else {
		m_coordx = rand() % (WIDTH - 30);
		m_coordy = -rand() % 250-50;
		m_dy = rand() % 4 + 2;
	}
	m_name = ObjectName::ASTEROID;
	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();
	m_gameConfig->get("AsteroidTexturePath", AsteroidTexturePath);
	if (!m_texture.loadFromFile(AsteroidTexturePath))
		std::cerr << "Error while loading the texture of Asteroid \n";
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);
	m_sprite.setTextureRect(sf::IntRect(0, 0, 40, 40));
	m_textureX = 0;
	m_textureY = 0;
	m_health = 1;
	m_sprite.setPosition(m_coordx, m_coordy);
}

Asteroid::Asteroid(float coordx, float coordy, float radius, float angle, float health):
	Object(coordx, coordy, rand() % 8 - 4, rand() % 8 - 4, radius, angle, health)
{
	m_name = ObjectName::ASTEROID;
}

void Asteroid::Update()
{
	m_coordx += m_dx;
	m_coordy += m_dy;
	if (m_mod == 0 && m_coordx > WIDTH + 10)
		m_health = 0;
	else if (m_mod == 1 && m_coordx < -60)
		m_health = 0;
	else if (m_coordy > HEIGHT + 10)
		m_health = 0;
	if (m_textureX == 40 * 7)
		if (m_textureY == 40 * 7)
		{
			m_textureX = 0;
			m_textureY = 0;
		}
		else {
			m_textureX = 0;
			m_textureY += 40;
		}
	else
		m_textureX += 40;
	m_sprite.setTextureRect(sf::IntRect(m_textureX, m_textureY, 40, 40));

	m_sprite.setPosition(m_coordx, m_coordy);
}

void Asteroid::Draw()
{
}
