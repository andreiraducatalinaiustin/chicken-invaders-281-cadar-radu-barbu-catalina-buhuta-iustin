#include "MusicPlayer.h"

MusicPlayer::MusicPlayer(std::unique_ptr<GameConfig> &m_gameConfig)
{
	std::string MainThemePath, MainTheme2Path, BossFightPath, GameOverPath,  VictoryPath, 
		HitBuffer, RedHitBuffer, BlueHitBuffer, GreenHitBuffer, PlayerDamageBuffer, AsteroidDamageBuffer, 
		CatchWingBuffer;

	if (!m_gameConfig)
		m_gameConfig = std::make_unique<GameConfig>();

	m_gameConfig->get("MainThemePath", MainThemePath);
	m_mainTheme.openFromFile(MainThemePath);

	m_gameConfig->get("MainTheme2Path", MainTheme2Path);
	m_mainTheme2.openFromFile(MainTheme2Path);

	m_gameConfig->get("BossFightPath", BossFightPath);
	m_bossFight.openFromFile(BossFightPath);

	m_gameConfig->get("GameOverPath", GameOverPath);
	m_gameOver.openFromFile(GameOverPath);

	m_gameConfig->get("VictoryPath", VictoryPath);
	m_victory.openFromFile(VictoryPath);

	m_gameConfig->get("PlayerDamageBufferPath", PlayerDamageBuffer);
	m_gameConfig->get("AsteroidDamageBufferPath", AsteroidDamageBuffer);
	m_gameConfig->get("HitBufferPath", HitBuffer);
	m_gameConfig->get("RedBulletBufferPath", RedHitBuffer);
	m_gameConfig->get("BlueBulletBufferPath", BlueHitBuffer);
	m_gameConfig->get("GreenHitBufferPath", GreenHitBuffer);
	m_gameConfig->get("CatchWingBufferPath", CatchWingBuffer);

	m_playerDamageBuffer.loadFromFile(PlayerDamageBuffer);
	m_asteroidDamageBuffer.loadFromFile(AsteroidDamageBuffer);
	m_hitBuffer.loadFromFile(HitBuffer);
	m_redBulletBuffer.loadFromFile(RedHitBuffer);
	m_blueBulletBuffer.loadFromFile(BlueHitBuffer);
	m_greenBulletBuffer.loadFromFile(GreenHitBuffer);
	m_catchWingBuffer.loadFromFile(CatchWingBuffer);
	
}

void MusicPlayer::PlayMainTheme()
{
	m_mainTheme.setLoop(true);
	m_mainTheme.play();
}

void MusicPlayer::PauseMainTheme()
{
	m_mainTheme.pause();
}

void MusicPlayer::StopMainTheme()
{
	m_mainTheme.stop();
}

void MusicPlayer::PlayBossFight()
{
	m_bossFight.play();
}

void MusicPlayer::StopBossFight()
{
	m_bossFight.stop();
}

void MusicPlayer::PlayGameOver()
{
	m_gameOver.play();
}

void MusicPlayer::StopGameOver()
{
	m_gameOver.stop();
}

void MusicPlayer::PlayVictory()
{
	m_victory.play();
}

void MusicPlayer::StopVictory()
{
	m_victory.stop();
}

void MusicPlayer::PlayHit()
{
	m_usedSound.setBuffer(m_hitBuffer);
	m_usedSound.play();
}

void MusicPlayer::PlayRedBullet()
{
	m_usedSound.setBuffer(m_redBulletBuffer);
	m_usedSound.setVolume(15);
	m_usedSound.play();
}

void MusicPlayer::PlayBlueBullet()
{
	m_usedSound.setBuffer(m_blueBulletBuffer);
	m_usedSound.play();
}

void MusicPlayer::PlayGreenBullet()
{
	m_usedSound.setBuffer(m_greenBulletBuffer);
	m_usedSound.play();
}

void MusicPlayer::PlayCatchWing()
{
	m_usedSound.setBuffer(m_catchWingBuffer);
	m_usedSound.play();
}
